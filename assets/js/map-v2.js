function CoMap(options=null){
    /* private properties ==========> */
    var _context = this;
    var _map = null;
    var _options = {
        parentContainer: '#mapContainer',
        parentContainerName: 'mapContainer',
        container: '#mapContainerMap',
        hideContainer: 'search-content',
        data: {},
        arrayBounds: [],
        bounds: null,
        markersCluster: null,
        markerList: {},
        popUpBtnClass: "",
        geoJSONlayer: null,
        activeCluster: true,
        activePopUp: false,
        activePreview: false,
        listItem: true,
        polylineList: {},
        distanceToMax: 0,
        elts:null,
        mapOpt:{
            dragging: true,
            draggable: false,
            center: [0, 0],
            centerOneZoom:18,
            zoom: 10,
            offset: {
                x: 0,
                y: 0
            },
            menuRight: null,
            btnHide: false,
            minZoom: 0,
            maxZoom: 18,
            mouseOver : false,
            showPopUpMouseOver : true,
            doubleClick : false,
            onclickMarker: null
        },
        mapCustom:{
            tile:"toner",
            css:[],
            markers: {
                default: modules.map.assets + '/images/markers/citizen-marker-default.png',
                organization: modules.map.assets + '/images/markers/ngo-marker-default.png',
                classified: modules.map.assets + '/images/markers/classified-marker-default.png',
                proposal: modules.map.assets + '/images/markers/proposal-marker-default.png',
                poi: modules.map.assets + '/images/markers/poi-marker-default.png',
                project: modules.map.assets + '/images/markers/project-marker-default.png',
                event: modules.map.assets + '/images/markers/event-marker-default.png',
                answer: modules.map.assets + '/images/markers/services/tools-hardware.png',
                getMarker: function(data){
                    var markers = _options.mapCustom.markers;
                    var collectionGroups = {
                        organization: ["organization", "organizations", "NGO", "Cooperative"],
                        classified: ["classified", "classifieds"],
                        proposal: ["proposal", "proposals"],
                        poi: ["poi"],
                        project: ["project", "projects"],
                        event: ["event", "events"],
                        answer: ["answers"]
                    }
                    var marker = markers.default;

                    if(data){
                        if(data.profilMarkerImageUrl)
                            marker = data.profilMarkerImageUrl;
                        else if(data.collection){
                            $.each(collectionGroups, function(k, v){
                                if(v.indexOf(data.collection) > -1){
                                    marker = markers[k]
                                }
                            })
                        }
                    }

                    return marker;
                }
            },
            icon:{
                options:{
                    iconSize: [45, 55],
                    iconAnchor: [25, 45],
                    popupAnchor: [-3, -30],
                    shadowUrl: '',
                    shadowSize: [68, 95],
                    shadowAnchor: [22, 94]
                },
                getIcon:function(params){
                    var iconOptions = _options.mapCustom.icon.options;
                    iconOptions.iconUrl = _options.mapCustom.markers.getMarker(params.elt);

                    if(typeof params.elt.osmId != "undefined") 
                        iconOptions.className = "osm-marker-"+params.elt.osmId;
                    
                    if(typeof params.elt != "undefined" && typeof params.elt.profilMediumImageUrl != "undefined"){
                        return L.divIcon({
                            className: "image-div-marker",
                            iconAnchor: [23, 45],
                            iconSize: [45, 45],
                            labelAnchor: [-45, 0],
                            popupAnchor: [0, -45],
                            html: `<div class='marker-image'></div><img src="${baseUrl + params.elt.profilMediumImageUrl}">`
                        });
                    }

                    return L.icon(iconOptions);
                }
            },
            lists: {
                text:"",
                createItemList: function(kElt, element, container){
                    if(element.type && $.inArray(element.type, _filtres.types) == -1){
                        _filtres.types.push(element.type)
                        var valueIdType = element.type.replace(/[^A-Za-z0-9]/g, "");
                        if (!$('#item_panel_map_' + valueIdType).length){
                            var newItem = "<button class='item_panel_map' id='item_panel_map_" + valueIdType + "'>" +
                                        "<i class='fa fa-tag' ></i> " + element.type +
                                        "</button>";
                            $(_options.parentContainer +' .panel_filter').append(newItem);

                            $("#item_panel_map_" + valueIdType).click(function(){
                                $(".item_panel_map").removeClass("active");
                                $("#item_panel_map_" + valueIdType).addClass("active");
                                _filtres.typesActived = []
                                if($.inArray(valueIdType, _filtres.typesActived) == -1){
                                    _filtres.typesActived.push(valueIdType)
                                }
                                _context.clearMap()
                                _context.addElts(_options.data)
                            })
                        }
                    }
                    //prépare le nom (ou "anonyme")
                    var name = (element.name)?element.name:(element.title?element.title:"Anonyme")
                    //recuperation de l'image de profil (ou image par defaut)
                    var imgProfilPath = _options.mapCustom.getThumbProfil(element)
                    var button = '<div class="element-right-list" id="element-right-list-' + kElt + '">' +
                                '<button class="item_map_list item_map_list_' + kElt + '">' +
                                "<div class='left-col'>" +
                                "<div class='thumbnail-profil'><img class='lzy_img' height=50 width=50 src='" + modules.co2.url +"/images/thumbnail-default.jpg' data-src='" + imgProfilPath + "'></div>" +
                                "</div>" +
                                "<div class='right-col'>" +
                                "<div class='info_item pseudo_item_map_list'>" + name + "</div>";
                    if(element.tags){
                        button += "<div class='info_item items_map_list'>";
                        var totalTags = 0;
                        $.each(element.tags, function(index, value){
                            totalTags++;
                            if(totalTags < 4){
                                var t = tradCategory[value]?tradCategory[value]:value;
                                button += "<a href='javascript:' class='tag_item_map_list'>#" + t + " </a>";
                            }
                            var valueId = value.replace(/[^A-Za-z0-9]/g, "");
                            //si l'item n'existe pas deja
                            if (!$('#item_panel_map_' + valueId).length){
                                var newItem = "<button class='item_panel_map' id='item_panel_map_" + valueId + "'>" +
								                "<i class='fa fa-tag' ></i> " + value +
                                            "</button>";
                                $(_options.parentContainer + ' .panel_map2').append(newItem);

                                $("#item_panel_map_"+valueId).click(function(){
                                    $(".item_panel_map").removeClass("active");
                                    $("#item_panel_map_" + valueId).addClass("active");
                                    
                                    _filtres.tagsActived = [];
                                    if($.inArray(value, _filtres.tagsActived) == -1){
                                        _filtres.tagsActived.push(value)
                                    }
                                    _context.clearMap()
                                    _context.addElts(_options.data)
                                })
                            }
                            if($.inArray(value, _filtres.tags) == -1)
                                _filtres.tags.push(value)
                        })
                        button += "</div>";
                    }

                    if(element.address){
                        button += "<div class='info_item city_item_map_list inline'>";
                        if(element.address.addressLocality){
                            button += element.address.addressLocality + " "
                        }
                        if(element.address.postalCode){
                            button += element.address.postalCode + " "
                        }
                        if(element.address.addressCountry){
                            button += element.address.addressCountry + " "
                        }
                        button += "</div>";
                    }
                    button += "</div>";

                    if(element.type == "event" || element.type == "events"){
                        button += displayStartAndEndDate(element);
                    }
                    button += '<div class="col-xs-12 separation"></div>' +
                                '</button>' +
                            '</div>';
                    $(_options.parentContainer + " .liste_map_element").append(button)
                
                    $(_options.parentContainer + " .item_map_list_" + kElt).click(function(){
                        if(element.geo && element.geo.latitude && element.geo.longitude){
                            var m = _options.markerList[kElt]
                            _options.markersCluster.zoomToShowLayer(m, function(){
                                m.openPopup()
                                coInterface.bindLBHLinks();
                            })
                        }else{
                            $(_options.parentContainer + " .modalItemNotLocated").modal('show');
                            $(_options.parentContainer + " .modalItemNotLocated .modal-body").html("<i class='fa fa-spin fa-reload'></i>");
                            $(_options.parentContainer + " .modalItemNotLocated .modal-body").html(_options.mapCustom.getPopup(element))
                            $(_options.parentContainer + " .modalItemNotLocated #btn-open-details").click(function(){
                                $("#popup" + kElt).click();
                                if(networkJson && networkJson.dataSrc){
                                    urlCtrl.loadByHash("'+url+'");
                                }
                            })

                            $(_options.parentContainer + " .modalItemNotLocated #popup" + kElt).click(function(){
                                $(_options.parentContainer + " .modalItemNotLocated").modal('hide')
                            })
                        }
                    })
                }
            },
            getClusterIcon: function(cluster){
                var childCount = cluster.getChildCount();
                var c = ' marker-cluster-';
                if (childCount < 10) {
                    c += 'small';
                } else if (childCount < 100) {
                    c += 'medium';
                } else {
                    c += 'large';
                }
                return L.divIcon({ html: '<div>' + childCount + '</div>', className: 'marker-cluster' + c, iconSize: new L.Point(40, 40) });
            },
            getThumbProfil: function (data) {
                var imgProfilPath = modules.map.assets + "/images/thumb/default.png";
                if (typeof data.profilThumbImageUrl !== "undefined" && data.profilThumbImageUrl != "")
                    imgProfilPath = baseUrl + data.profilThumbImageUrl;
                else
                    imgProfilPath = modules.map.assets + "/images/thumb/default_" + data.collection + ".png";
    
                return imgProfilPath;
            },
            getPopup: function(data){
                var id = data._id ? data._id.$id:data.id;
                var imgProfil = _options.mapCustom.getThumbProfil(data)

                var eltName = data.title ? data.title: ((typeof data.properties != "undefined" && typeof data.properties.name != "undefined") ? data.properties.name : data.name);
                var popup = "";
                popup += "<div class='padding-5' id='popup" + id + "'>";
                if(data.imgProfil){
                    popup += "<img src='" + imgProfil + "' height='30' width='30' class='' style='display: inline; vertical-align: middle; border-radius:100%;'>";
                }
                let marginLeftTitle = (typeof data.isOsmData == "undefined") ? "5px" : "22%";
                popup += "<span style='margin-left : 5px; font-size:18px'>" + eltName + "</span>";

                if(data.tags && data.tags.length > 0){
				    popup += "<div style='margin-top : 5px;'>";
                    var totalTags = 0;
                    $.each(data.tags, function(index, value){
                        totalTags++;
                        if (totalTags < 3) {
                            popup += "<div class='popup-tags'>#" + value + " </div>";
                        }
                    })
                    popup += "</div>";
                }
                if(data.address){
                    var addressStr="";
                    if(data.address.streetAddress)
                        addressStr += data.address.streetAddress;
                    if(data.address.postalCode)
                        addressStr += ((addressStr != "")?", ":"") + data.address.postalCode;
                    if(data.address.addressLocality)
                        addressStr += ((addressStr != "")?", ":"") + data.address.addressLocality;
                    if(!data.address.addressLocality && !data.address.streetAddress) {
                        if(data.address.level4Name)
                            addressStr += ((addressStr != "")?", ":"") + data.address.level4Name;
                        if(data.address.level3Name)
                            addressStr += ((addressStr != "")?", ":"") + data.address.level3Name;
                        if(data.address.level1Name)
                            addressStr += ((addressStr != "")?", ":"") + data.address.level1Name;
                    }
                    popup += "<div class='popup-address text-dark'>";
                    popup += 	"<i class='fa fa-map-marker'></i> "+addressStr;
                    popup += "</div>";
                }
                if(data.shortDescription && data.shortDescription != ""){
                    popup += "<div class='popup-section'>";
					popup += "<div class='popup-subtitle'>Description</div>";
					popup += "<div class='popup-shortDescription'>" + data.shortDescription + "</div>";
				    popup += "</div>";
                }
                if((data.url && typeof data.url == "string") || data.email || data.telephone){
                    popup += "<div id='pop-contacts' class='popup-section'>";
                    popup += "<div class='popup-subtitle'>Contacts</div>";
                    
                    if(data.url && typeof data.url === "string"){
                        popup += "<div class='popup-info-profil'>";
                        popup += "<i class='fa fa fa-desktop fa_url'></i> ";
                        popup += "<a href='" + data.url + "' target='_blank'>" + data.url + "</a>";
                        popup += "</div>";
                    }

                    if(data.email){
                        popup += "<div class='popup-info-profil'>";
                        popup += "<i class='fa fa-envelope fa_email'></i> " + data.email;
                        popup += "</div>";
                    }

                    if(data.telephone){
                        popup += "<div class='popup-info-profil'>";
                        popup += "<i class='fa fa-phone fa_phone'></i> ";
                        var tel = ["fixe", "mobile"];
                        var iT = 0;
                        $.each(tel, function(keyT, valT){
                            if(data.telephone[valT]){
                                $.each(data.telephone[valT], function(keyN, valN){
                                    if(iT > 0)
                                        popup += ", ";
                                    popup += valN;
                                    iT++; 
                                })
                            }
                        })
                        popup += "</div>";
                    }

                    popup += "</div>";
				    popup += "</div>";
                }
                if(data.collection && id && typeof data.isOsmData == "undefined"){
                    var url = '#page.type.' + data.collection + '.id.' + id;
                    popup += "<div class='popup-section'>";
                    if(_options.activePreview)
                    popup += "<a href='" + url + "' class='lbh-preview-element "+_options.mapCustom.popUpBtnClass+" item_map_list popup-marker' id='popup" + id + "'>";
                    else
                    popup += "<a href='" + url + "' target='_blank' class='lbh "+_options.mapCustom.popUpBtnClass+" item_map_list popup-marker' id='popup" + id + "'>";
                    popup += '<div class="btn btn-sm btn-more col-md-12">';
                    popup += '<i class="fa fa-hand-pointer-o"></i>' + trad.knowmore;
                    popup += '</div></a>';
                }
                else if (typeof data.isOsmData != "undefined") {
                    var action = (typeof data.actionBtn != "undefined" && data.actionBtn != null && typeof data.actionBtn.action != "undefined") ? data.actionBtn.action : "openPreviewOsm";
                    let id = (typeof data.osmId != "undefined") ? data.osmId : "";
                   
                    popup += `<div data-id="`+id+`"  style='cursor:pointer;' onClick="`+action+`('osmPopupBtn` + id + `')" class='popup-marker' id='osmPopupBtn` + id + `'>`;
                    popup += '<div class="btn btn-sm btn-more col-md-12">';
                    popup += '<i class="fa fa-hand-pointer-o"></i>' + trad.knowmore;
                    popup += '</div></div>';
                }
                popup += '</div>';
                popup += '</div>';

                if(typeof data.isOsmData != "undefined") {
                    let srcImage = (data.isOsmData) ? modules.co2.url + "/images/osm.jpeg" : modules.co2.url + "/images/CO.png";
                    let color = (data.isOsmData) ? "#086784" : "#846708";

                    popup = `
                        <div  style="position: relative;overflow: hidden;">
                            <div style="position: absolute;top:0;right:0;width: 30px;height: 30px;z-index: 1;border: solid 2px rgba(0, 0, 0, 0.2);border-radius: 50%;overflow: hidden;transition: .3s;">
                                <img style="width: 100%;object-fit: contain;height: auto;background-color: #ffffff;" src="` + srcImage + `">
                            </div>
                            <div style="margin-top:10px;margin-right:30px;">`+ popup +`</div>
                        </div>
                            `;
                            //<div style="width:100px;height: 100px;position: absolute;top:-60px;right:-60px;transform: rotate(40deg);background: `+color+`;"></div>
                }

                return popup;
            },
            addMarker:null,
            addElts:null
        },
        buttons:{
            fullScreen:{
                allow:false,
                position: "bottomLeft",
                icon:{
                    maximize: 'expand',
                    minimize: 'compress'
                },
                style:{
                    default:{
                        border: "none",
                        borderRadius: "100%",
                        width: "40px",
                        height: "40px",
                        background: "rgba(255,255,255,.5)",
                        outline:"none"
                    },
                    onHover:{
                        background: "rgba(255,255,255,1)"
                    }
                }
            }
        },
        actionButtons:[]
    }
    var _utils = {
        bindMap: function(){
            $(_options.parentContainer + " .input_name_filter").keyup(function(){
                if (notNull(_filtres.timeoutAddCity))
                    clearTimeout(_filtres.timeoutAddCity);
                
                _filtres.timeoutAddCity = setTimeout(function () {
					_filtres.search = $(_options.parentContainer + " .input_name_filter").val();
                    _context.clearMap();
                    _context.addElts(_options.data);
				}, 500);
            })

            $(_options.parentContainer + " .item_panel_filter_all").off().click(function(){
                $(_options.parentContainer + " .item_panel_map").removeClass("active");
                $(_options.parentContainer + " .item_panel_filter_all").addClass("active");
                _filtres.typesActived = [];
                _context.clearMap();
                _context.addElts(_options.data)
            })

            $(_options.parentContainer + " .item_panel_map_all").off().click(function(){
                _filtres.tagsActived = [];
                $(_options.parentContainer  + " .item_panel_map").removeClass("active")
                $(_options.parentContainer + " .item_panel_map_all").addClass("active")
                _context.clearMap();
                _context.addElts(_options.data)
            })

            $(_options.parentContainer + " .btn-hide-map").off().click(function(){
                showMap(false);
            })

            $(_options.parentContainer + " #btn-close-menuRight").off().click(function(){
                if(!$(this).data("close")){
                    $("#menuRight"+ _options.parentContainerName).hide();
                    $(this).html('<i class="fa fa-angle-left"></i>');
                    $(_options.parentContainer + " .closeMenuRight").css('right', "0px");
                    $(this).data("close", true);
                }else{
                    $("#menuRight"+ _options.parentContainerName).show();
                    $(this).html('<i class="fa fa-angle-right"></i>');
                    var right = "341px";
                    if(_options.mapOpt.menuRight && _options.mapOpt.menuRight.btnClose && _options.mapOpt.btnClose.right){
                        right = _options.mapOpt.btnClose.right;
                    }
                    $(_options.parentContainer + " .closeMenuRight").css('right', right);
                    $(this).data("close", false);
                }
            })
        },
        generateRandomId: function(){
            return Math.random().toString(36).replace(/[^a-z]+/g, '').substr(2, 10);
        }
    }
    var _filtres = {
        types: [],
        typesActived: [],
        tags: [],
        tagsActived: [],
        search: "",
        result: function(elt){
            var res = _filtres.isTags(elt) && _filtres.isSearch(elt) && _filtres.isType(elt)
            return res;
        },
        isTags: function(elt){
            var res = false;
            if(Object.keys(_filtres.tagsActived).length == 0 || _filtres.tagsActived || _filtres.tagsActived.length == 0){
                res = true;
            }else{
                $.each(_filtres.tagsActived, function(k, v){
                    if(elt.tags && $.inArray(v, elt.tags) > -1)
                        res = true;
                })
            }
            return res;
        },
        isType: function(elt){
            var res = false;
            if(Object.keys(_filtres.typesActived).length == 0 || _filtres.typesActived || _filtres.typesActived.length == 0){
                res = true
            }else{
                $.each(_filtres.typesActived, function(k, v){
                    if(elt.type && v == elt.type)
                        res = true;
                })
            }
            return res;
        },
        isSearch: function(elt){
            var res = false;
            if(_filtres.search){
                if(elt.name && elt.name.search(new RegExp(_filtres.search, "i")) >= 0)
                    res = true;
            }else{
                res = true
            }
            return res;
        }
    }
    /* <========== private properties */

    /* private methodes ==========> */
    var initVar = function(){
        if(options){
            _options = $.extend(true, {}, _options, options)

            _options.parentContainer = (options.container) ? options.container:"#mapContainer";
            _options.parentContainerName = _options.parentContainer.replace("#", "").replace(".","")
            _options.container = _options.parentContainer + "Map";
            // _options.geoJSONlayer = new L.FeatureGroup();

            if(_options.activeCluster){
                _options.markersCluster = new L.markerClusterGroup({
                    iconCreateFunction: function (cluster) {
                        return _options.mapCustom.getClusterIcon(cluster);
                    }
                })
            }
        }
    }

    var initView = function(){
        var view = "";

        view += "<div class='mapLoading'></div>";
        if(_options.mapOpt.menuRight){
            var dataCloseMenuRight = false,
                sizeList = $(_options.parentContainer).height() - 80;

            if(_options.mapOpt.menuRight.btnClose){
                view += '<div class="closeMenuRight">'+
                            '<button type="button" data-close="'+dataCloseMenuRight+'" class="btn btn-default " id="btn-close-menuRight" data-toggle="dropdown">' +
                                '<i class="fa fa-angle-right"></i>' +
                            '</button>' +
                        '</div>';
            }

            view += '<div id="menuRight' + _options.parentContainerName + '" class="hidden-xs col-sm-3 no-padding menuRight" style="">' +
					'<div id="menuRight_search' + _options.parentContainerName + '" class="col-sm-12 no-padding menuRight_search">' +
					'<div class="menuRight_header col-sm-12 padding-10">' +
					'<div class="col-xs-6">' +
					'<span id="result' + _options.parentContainerName + '" class=""></span>' +
					'<span class="menuRight_header_title"> '+tradMap.Results+' </span>' +
                '</div>';
            
            if(_options.mapOpt.btnHide){
                view += '<div class="col-xs-2 pull-right">' +
					'<button type="button" class="btn btnHeader dropdown-toggle btn-hide-map" data-toggle="dropdown">' +
					'<i class="fa fa-times"></i>' +
					'</button>' +
					'</div>';
            }
            view += '<div class="col-xs-2 pull-right">' +
				'<button type="button" class="btn btnHeader dropdown-toggle" id="btn-filters' + _options.parentContainerName + '" data-toggle="dropdown">' +
				'<i class="fa fa-filter"></i>' +
				'</button>' +
				'<ul class="dropdown-menu panel_filter panel_map"  role="menu" aria-labelledby="panel_filter" style="overflow-y: auto; max-height: 400px;">' +
				'<h3 class="title_panel_map"><i class="fa fa-angle-down"></i> '+tradMap['Filter results by types']+'</h3>' +
				'<button class="item_panel_map active item_panel_filter_all" >' +
				'<i class="fa fa-star"></i> All' +
				'</button>' +
				'</ul>' +
				'</div>' +
				'<div class="col-xs-2 pull-right">' +
				'<button type="button" class="btn btnHeader dropdown-toggle" id="btn-panel' + _options.parentContainerName + '" data-toggle="dropdown">' +
				'<i class="fa fa-tags"></i>' +
				'</button>' +
				'<ul class="dropdown-menu panel_map panel_map2 pull-right" role="menu" aria-labelledby="panel_map" style="overflow-y: auto; max-height: 400px;">' +
				'<h3 class="title_panel_map"><i class="fa fa-angle-down"></i> '+tradMap['Filter results by tags']+'</h3>' +
				'<button class="item_panel_map active item_panel_map_all" >' +
				'<i class="fa fa-star"></i> All' +
				'</button>' +
				'</ul>' +
				'</div>' +
				'</div>' +


				'<div id="menuRight_search_filters' + _options.parentContainerName + '" class="col-sm-12 no-padding menuRight_search_filters">' +
				'<input class="form-control date-range active input_name_filter" type="text" placeholder="'+tradMap['Filter by names']+'">' +
				'</div>' +
				'<div class="menuRight_body col-sm-12 no-padding liste_map_element" ' +
				' style="height: ' + sizeList + 'px;"></div>' +
				'</div>' +
                '</div>';
            
            view += '<div class="modal fade modalItemNotLocated" role="dialog">' +
				'<div class="modal-dialog">' +
				'<div class="modal-content" style="top: 125px;">' +
				'<div class="modal-header">' +
				'<button type="button" class="close" data-dismiss="modal">&times;</button>' +
				'<h4 class="modal-title text-dark">' +
				'<i class="fa fa-map-marker"></i>' + trad["This data is not geolocated"] +
				'</h4>' +
				'</div>' +
				'<div class="modal-body"></div>' +
				'<div class="modal-footer" style="border-top: none;">' +
				'<button type="button" class="btn btn-default btn-sm btn-danger" data-dismiss="modal"><i class="fa fa-times"></i>' + trad.closed + '</button>' +
				'</div>' +
				'</div>' +
				'</div>' +
				'</div>';
        }else{
            if (_options.mapOpt.btnHide) {
				view += '<div class="col-xs-2 div-btn-hide">' +
					'<button type="button" class="btn btnHeader dropdown-toggle btn-hide-map" id="" data-toggle="dropdown" style="background-color: #e6434d; color: white;">' +
					'<i class="fa fa-times"></i>' +
					'</button>' +
					'</div>';
			}
        }
        // view += '<div id="' + _options.container + '" style="z-index: 1; width: 100%; position: fixed; bottom:0; top:inherit;"></div>';
        view += '<div id="' + _options.container + '" style="z-index: 1; width: 100%; height: 100%; position:relative;"></div>';

        $(_options.parentContainer).html(view);

        if(_options.mapOpt.menuRight && _options.mapOpt.menuRight.close){
            $("#menuRight"+ _options.parentContainerName).hide();
            $(_options.parentContainer + " #btn-close-menuRight").html('<i class="fa fa-angle-left"></i>');
            $(_options.parentContainer + " .closeMenuRight").css('right', "0px");
            $(_options.parentContainer + " #btn-close-menuRight").data("close", true);
        }

        if(_options.buttons.fullScreen.allow)
            initBtnFullScreen();
        
        if(_options.actionButtons && Array.isArray(_options.actionButtons) && _options.actionButtons.length > 0){
            initBtnActions()
        }
    }

    var initBtnFullScreen = function(){
        var optBtnFullScreen = _options.buttons.fullScreen,
            fullScreenActived = false,
            $parentContainer = $(_options.parentContainer),
            buttonContainerId = _options.parentContainerName + "-btn-fullscreen-container",
            defaultSizeParentContainer = {
                width: $parentContainer.width(),
                height: $parentContainer.height()
            }

        var btnContainerPositionOptions = {
            topLeft: { left:0, top:0 },
            bottomLeft: { left:0, bottom:0 },
            topRight: { top:0, right:0 },
            bottomRight: { bottom:0, right:0 },
            default: { left:0, bottom:0 }
        }
        var btnContainerPosition = (btnContainerPositionOptions[optBtnFullScreen.position])? 
                                    btnContainerPositionOptions[optBtnFullScreen.position]:
                                    btnContainerPositionOptions.default;
        var btnContainerStyle = {
            position:"absolute",
            zIndex: 100000,
            padding:"20px"
        }
        $.each(btnContainerPosition, function(property, value){
            btnContainerStyle[property] = value;
        })

        var button = `
            <div id="${buttonContainerId}">
                <button><i class="fa fa-${optBtnFullScreen.icon.maximize}" aria-hidden="true"></i></button>
            </div>
        `
        $parentContainer.append(button)

        var $buttonContainerId = $("#"+buttonContainerId),
            $button = $buttonContainerId.find("button");
        
        $buttonContainerId.css(btnContainerStyle)
        $button.css(optBtnFullScreen.style.default)
        $button.hover(function(){
            $button.css(optBtnFullScreen.style.onHover)
        }, function(){
            $button.css(optBtnFullScreen.style.default)
        })

        $button.click(function(){
            if(fullScreenActived){
                $parentContainer.css({
                    position:"relative",
                    width:defaultSizeParentContainer.width,
                    height:defaultSizeParentContainer.height
                })
                $("html, body").css({
                    overflow:"visible"
                });
                $button.html(`<i class="fa fa-${optBtnFullScreen.icon.maximize}" aria-hidden="true"></i>`)
                $("html, body").scrollTop($parentContainer.offset().top - 100)
            }else{
                var mainNavHeight = document.getElementById("mainNav").offsetHeight
                $parentContainer.css({
                    position:"absolute",
                    height:`calc(100vh - ${mainNavHeight}px)`,
                    width:"100vw",
                    top:0,
                    left:0,
                    zIndex:999
                })
                $("html, body").scrollTop(0).css({
                    overflow:"hidden"
                });
                $button.html(`<i class="fa fa-${optBtnFullScreen.icon.minimize}" aria-hidden="true"></i>`)
            }

            setTimeout(function(){
                _map.invalidateSize()
            }, 400)

            fullScreenActived = !fullScreenActived;
        })
    }

    var initBtnActions = function(){
        var btnActionItems = "";
        for(let i in _options.actionButtons){
            _options.actionButtons[i].id = `btn-map-action_${_utils.generateRandomId()}`;
            btnActionItems += `<li id="${_options.actionButtons[i].id}"><i class="fa fa-${_options.actionButtons[i].icon}" aria-hidden="true"></i></li>`
        }

        var btnActions = `
            <div class="btn-map-actions-container">
                <ul class="btn-map-actions">
                    ${btnActionItems}
                </ul>
                <button id="btn-toggle-map-actions"><i class="fa fa-plus" aria-hidden="true"></i></button>
            </div>
        `
        $(_options.parentContainer).append(btnActions);

        $('#btn-toggle-map-actions').click(function(){
            $(this).toggleClass('active')
            $(".btn-map-actions").toggleClass('active')
        })
        for(let i in _options.actionButtons){
            $("#"+ _options.actionButtons[i].id).click(function(){
                if(_options.actionButtons[i].onclick){
                    _options.actionButtons[i].onclick()
                }
                $('#btn-toggle-map-actions').removeClass("active")
                $(".btn-map-actions").removeClass('active')
            })
        }
    }

    var init = function(){
        if($(options.container).length > 0){
            initVar()
            initView()
            _context.showLoader();

            _map = L.map(_options.container, _options.mapOpt);
            _context.setTile(_options.mapCustom.tile)

            if(_options.elts){
                _context.addElts(_options.elts)
            }else{
                setTimeout(function(){
                    _map.invalidateSize()
                }, 400)
            }
        }
    }
    /* <========== private methodes */

    /* getters & setters ==========> */
    this.getMap = function(){
        return _map;
    }
    this.getOptions = function(){
        return _options;
    }
    this.setOptions = function(newOptions){
        _options = newOptions;
    }
    this.getUtils = function(){
        return _utils;
    }
    this.getFiltres = function(){
        return _filtres;
    }
    /* <========== getters & setters */

    /* public methodes ==========> */
    this.addElts = function(data){
        if(_options.mapCustom.addElts){
            _options.mapCustom.addElts(_context, data)
        }else{

            var geoJson = [];
            $.each(data, function(k, v){
                if(!_options.data[k])
                    _options.data[k] = v;
                if(typeof v.geoShape != "undefined"){
                    var data = $.extend({}, v);
                    var geoData = {
                            type: "Feature",
                            properties: {
                                name: v.name
                            },
                            geometry: v.geoShape
                    };
                    delete data.name;
                    delete data.getShape;
                    $.each(data, function(key,value) {
                        if(typeof geoData.properties[key] == 'undefined'){
                            geoData.properties[key] = value;
                        }
                    })
                    geoJson.push(geoData);
                }
                if(_filtres.result(v) && typeof v.geoShape == "undefined" && typeof v.geo != "undefined"){
                    v.id = k;
                    _context.addMarker({ elt: v })
                    _options.mapCustom.lists.createItemList(k, v, _options.container)
                }
            })
    
            _options.geoJSONlayer = L.geoJSON(geoJson, {
                onEachFeature: function(feature, layer){
                    _context.addPopUp(layer, feature);
                    layer.on("mouseover", function(){
                        this.setStyle({
                            "fillOpacity" : 1,
                            "fillColor" : "#0000FF"
                        })
                    });
                    layer.on("mouseout", function(){
                        this.setStyle({
                            "fillOpacity" : 0.2,
                            "fillColor" : "#0000FF"
                        })
                    });
                },
                style: {
                    color: "#0000FF",
                    opacity: 0.5
                }
            });
            _map.addLayer(_options.geoJSONlayer);
            /* if(_options.arrayBounds.length > 0){
                var point = null;
                if(_options.arrayBounds.length == 1){
                    point = {
                        x: _options.arrayBounds[0][0],
                        y: _options.arrayBounds[0][1],
                    }
                }else if(_options.arrayBounds.length >1){
                    _options.bounds = L.bounds(_options.arrayBounds)
                    point = _options.bounds.getCenter()
                }
    
                if(point && !isNaN(point.x) && !isNaN(point.y)){
                    this.setZoomByDistance()
    
                    var lat = parseFloat(point.x) + ((parseFloat(point.x) < 0) ? -Math.abs(_options.mapOpt.offset.x):_options.mapOpt.offset.x);
                    var lon = parseFloat(point.y) + ((parseFloat(point.y) < 0) ? -Math.abs(_options.mapOpt.offset.y):_options.mapOpt.offset.y);
                    
                    _options.mapOpt.center = [lat, lon]
    
                    if(_options.forced && _options.forced.latLon && _options.forced.zoom)
                        _map.setView(_options.forced.latLon,  _options.forced.zoom)
                    else
                        _map.setView([lat, lon], _options.mapOpt.zoom)
                }
            } */
            //this.fitBounds()
    
            if(_options.activeCluster)
                _map.addLayer(_options.markersCluster)
            _utils.bindMap();
    
            if(_options.forced){
                if(_options.forced.latLon && _options.forced.zoom){
                    _map.setView(_options.forced.latLon, _options.forced.zoom)
                }else{
                    if(_options.forced.latLon)
                        _map.panTo(_options.forced.latLon)
                    if(_options.forced.zoom)
                        _map.setZoom(_options.forced.zoom)
                }
            }
    
            _context.hideLoader();
            _map.invalidateSize();
            _context.fitBounds(data);
        }
    }

    this.addPolyLine = function(data, style = {
        color: 'red',weight: 2,
        opacity: 0.5,
        smoothFactor: 1
    }){
        var polyline = L.polyline(data.coordinates, style
        );
        _options.polylineList[data.id] = polyline;
        polyline.addTo(_map);
    }
    this.addCurveLine = function(data, style = {
        color: 'red',weight: 2,
        opacity: 0.5,
        smoothFactor: 1
    }){
        var polyline = L.curve(data.coordinates, style);
        _options.polylineList[data.id] = polyline;
        polyline.addTo(_map);
    }

    this.addMarker = function(params){
        if(_options.mapCustom.addMarker){
            _options.mapCustom.addMarker(_context, params)
        }else{
            if( params.elt && params.elt.geo && params.elt.geo.latitude && params.elt.geo.longitude){
                if(!params.opt)
                    params.opt = {}
                
                params.opt.icon = _options.mapCustom.icon.getIcon(params)
    
                var latLon = [params.elt.geo.latitude, params.elt.geo.longitude];
                this.setDistanceToMax(latLon);
    
                var marker = L.marker(latLon, params.opt);
                _options.markerList[params.elt.id] = marker;
    
                if(_options.activePopUp){
                    _context.addPopUp(marker, params.elt)
                }
    
                _options.arrayBounds.push(latLon)
    
                if(_options.activeCluster){
                    _options.markersCluster.addLayer(marker)
                }else{
                    marker.addTo(_map);
                    if(params.center){
                        _map.panTo(latLon)
                    }
                }
    
                if(_options.mapOpt.doubleClick){
                    marker.on('click', function(e){
                        this.openPopup()
                        coInterface.bindLBHLinks();
                    })
                    marker.on('dbclick', function(e){
                        _context.centerOne(params.elt.geo.latitude, params.elt.geo.longitude)
                        coInterface.bindLBHLinks();
                    })
                }else{
                    marker.on('click', function (e) {
                        if(_options.mapOpt.onclickMarker){
                            _options.mapOpt.onclickMarker(params)
                        }else{
                            _context.centerOne(params.elt.geo.latitude, params.elt.geo.longitude)
                            coInterface.bindLBHLinks();
                        }
                    });
                }
    
                if(_options.mapOpt.mouseOver){
                    marker.on('mouseover', function (e) {
                        this.openPopup()
                        coInterface.bindLBHLinks();
                    });
                    marker.on('mouseout', function (e) {
                        var thismarker = this;
                        setTimeout(function(){
                            thismarker.closePopup();
                        }, 2000);
                    });
                }    
            }
        }
    }

    this.removeMarker = function(id){
        let data = $.extend({}, _options.markerList[id], true);
        let latlng = data.getLatLng();
        _map.removeLayer(data);
        delete _options.markerList[id];
        mylog.log("Lat lnt", latlng);
        _options.arrayBounds = _options.arrayBounds.filter(item => JSON.stringify(item) != JSON.stringify([latlng.lat.toString(), latlng.lng.toString()]));
        _map.invalidateSize();
        _context.fitBounds();
    }

    this.setDistanceToMax = function(latLon){
        if(Object.keys(_options.markerList).length > 0){
            var latLonObj = L.latLng(latLon[0], latLon[1]);
            $.each(_options.markerList, function(k, v){
                var dist = latLonObj.distanceTo(v.getLatLng())
                _options.distanceToMax = (dist > _options.distanceToMax) ? dist:_options.distanceToMax
            })
        }
    }

    this.setZoomByDistance = function(){
        var markerListCount = Object.keys(_options.markerList).length
        if(markerListCount == 0){
            _options.mapOpt.zoom = 0;
            _options.mapOpt.offset.x = 0;
            _options.mapOpt.offset.y = 0;
        }else if(markerListCount == 1){
            _options.mapOpt.zoom = 14;
			_options.mapOpt.offset.x = 0.002;
			_options.mapOpt.offset.y = 0.003;
        }else{
            if(_options.distanceToMax > 9000000){
                _options.mapOpt.zoom = 2;
				_options.mapOpt.offset.x = 0;
				_options.mapOpt.offset.y = 15;
            }else if(_options.distanceToMax > 7500000){
                _options.mapOpt.zoom = 3;
				_options.mapOpt.offset.x = 0;
				_options.mapOpt.offset.y = 15;
            }else if(_options.distanceToMax > 6000000){
                _options.mapOpt.zoom = 4;
				_options.mapOpt.offset.x = 0;
				_options.mapOpt.offset.y = 0.2;
            }else if(_options.distanceToMax > 50000){
                _options.mapOpt.zoom = 6;
				_options.mapOpt.offset.x = 0;
				_options.mapOpt.offset.y = 0.2;
            }else if(_options.distanceToMax > 20000){
                _options.mapOpt.zoom = 8;
				_options.mapOpt.offset.x = 0;
				_options.mapOpt.offset.y = 0.2;
            }else if(_options.distanceToMax > 10000){
                _options.mapOpt.zoom = 10;
				_options.mapOpt.offset.x = 0;
				_options.mapOpt.offset.y = 0.2;
            }else{
                _options.mapOpt.zoom = 12;
				_options.mapOpt.offset.x = 0;
				_options.mapOpt.offset.y = 0.2;
            }
        }

        if(!_options.mapOpt.menuRight){
            _options.mapOpt.offset.x = 0;
			_options.mapOpt.offset.y = 0;
        }
    }

    this.clearPolylines = function(){
        if(Object.keys(_options.polylineList).length > 0){
            $.each(_options.polylineList, function(){
                _map.removeLayer(this)
            })
        }
    }
    this.polylineVisible = function(id){
        if(Object.keys(_options.polylineList).includes(id)){
            let $polyline = _options.polylineList[id];
            return _map.hasLayer($polyline);
        }
        return false;
    }
    this.togglePolyline = function(id){
        if(Object.keys(_options.polylineList).includes(id)){
            let $polyline = _options.polylineList[id];
            if(_map.hasLayer($polyline)){
                _map.removeLayer($polyline);
            }else{
                _map.addLayer($polyline);
            }
        }
    }


    this.clearMap = function(){
        if(_options.markersCluster){
            _options.markersCluster.clearLayers()
        }
        if(_options.geoJSONlayer){
            _options.geoJSONlayer.clearLayers()
        }
        if(Object.keys(_options.markerList).length > 0){
            $.each(_options.markerList, function(){
                _map.removeLayer(this)
            })
        }
        if(Object.keys(_options.polylineList).length > 0){
            $.each(_options.polylineList, function(){
                _map.removeLayer(this)
            })
        }
        if(_options.markerSingleList && Object.keys(_options.markerSingleList)){
            $.each(_options.markerSingleList, function(){
                _map.removeLayer(this)
            })
        }
        _options.markerList = {};
        _options.arrayBounds = [];
        _options.bounds = null;

        _context.showLoader()
        $(_options.parentContainer + " .liste_map_element").html("")
    }

    this.addPopUp = function(marker, elt=null, open=false){
        var popupSimple = _options.mapCustom.getPopup(elt)
        marker.bindPopup(popupSimple, {width:"300px"}).openPopup();
        if(open)
            marker.openPopup();
    }

    this.centerOne = function(lat, lon){
        if(_filtres.timeoutAddCity)
            clearTimeout(_filtres.timeoutAddCity)
        _filtres.timeoutAddCity = setTimeout(function(){
            var moveLat = "0.0008";
            var moveLon = "0.000";
            
            lat = parseFloat(lat) + parseFloat(moveLat);
            lon = parseFloat(lon) + parseFloat(moveLon);
            
            var center = [lat, lon];

            _map.setView(center, _options.mapOpt.centerOneZoom)
        }, 500)
    }

    this.showLoader = function(){
        $(".mapLoading").show();
        var heightMap = $(_options.parentContainer).outerHeight();
        var widthMap = $(_options.parentContainer).outerWidth();
        if ($(_options.parentContainer + " #menuRight" + _options.parentContainerName).is(":visible")) {
			widthMap = widthMap - $(_options.parentContainer + " #menuRight" + _options.parentContainerName).outerWidth();
        }
        coInterface.showLoader(".mapLoading", trad.currentlyresearching);
        var marginLeftLoader = (widthMap - $(".mapLoading").outerWidth()) / 2;
        var marginTopLoader = (heightMap - $(".mapLoading").outerHeight()) / 2;
        $(".mapLoading").css({ "margin-left": marginLeftLoader + "px", "margin-top": marginTopLoader + "px" });
    }
    this.hideLoader = function(){
        $(".mapLoading").fadeOut();
    }

    this.setTile = function(tile){
        if (tile === "mapbox" && $('script[src="https://api.mapbox.com/mapbox.js/v3.2.1/mapbox.js"]').length){
            var accessToken = 'pk.eyJ1IjoiY29tbXVuZWN0ZXI5NzQiLCJhIjoiY2tkOHF3cTZ1MDZmNzJzbWk4M3pnZ2dvbCJ9.J9aOUObe8B9lH-8QLmDzNw';

            var mapboxTiles = L.tileLayer('https://api.mapbox.com/styles/v1/mapbox/streets-v11/tiles/{z}/{x}/{y}?access_token=' + accessToken, {
				attribution: '© <a href="https://www.mapbox.com/feedback/">Mapbox</a> © <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
            });
            _map.addLayer(mapboxTiles)
        } else if (tile === "maptiler" && $('script[src="https://api.mapbox.com/mapbox.js/v3.2.1/mapbox.js"]').length) {
            var accessToken = '2FAeSQOpVCo7WdsCmujj';

            var mapboxTiles = L.tileLayer('https://api.maptiler.com/maps/eb5fb1d0-58c0-47ab-af0a-86ed434e011c/256/{z}/{x}/{y}@2x.png?key=' + accessToken, {
                "attribution": "<a href=\"https://www.maptiler.com/copyright/\" target=\"_blank\">&copy; MapTiler</a> <a href=\"https://www.openstreetmap.org/copyright\" target=\"_blank\">&copy; OpenStreetMap contributors</a>"
            });
            _map.addLayer(mapboxTiles)
        }else if(tile === "toner-lite"){
            // L.tileLayer('https://tiles.stadiamaps.com/tiles/stamen_toner_lite/{z}/{x}/{y}{r}.png', {
			// 	attribution: '&copy; <a href="https://stadiamaps.com/" target="_blank">Stadia Maps</a> <a href="https://stamen.com/" target="_blank">&copy; Stamen Design</a> &copy; <a href="https://openmaptiles.org/" target="_blank">OpenMapTiles</a> &copy; <a href="https://www.openstreetmap.org/about" target="_blank">OpenStreetMap</a> contributors'
			// }).addTo(_map)

            var accessToken = '2FAeSQOpVCo7WdsCmujj';

			var mapboxTiles = L.tileLayer('https://api.maptiler.com/maps/toner-v2-lite/256/{z}/{x}/{y}@2x.png?key=' + accessToken, {
				"attribution": "<a href=\"https://www.maptiler.com/copyright/\" target=\"_blank\">&copy; MapTiler</a> <a href=\"https://www.openstreetmap.org/copyright\" target=\"_blank\">&copy; OpenStreetMap contributors</a>", 
			});
			_map.addLayer(mapboxTiles)
            
        }else if(tile === "satelite"){
            // L.tileLayer('https://tiles.stadiamaps.com/tiles/stamen_toner_lite/{z}/{x}/{y}{r}.png', {
			// 	attribution: '&copy; <a href="https://stadiamaps.com/" target="_blank">Stadia Maps</a> <a href="https://stamen.com/" target="_blank">&copy; Stamen Design</a> &copy; <a href="https://openmaptiles.org/" target="_blank">OpenMapTiles</a> &copy; <a href="https://www.openstreetmap.org/about" target="_blank">OpenStreetMap</a> contributors'
			// }).addTo(_map)

            var accessToken = '2FAeSQOpVCo7WdsCmujj';

			var mapboxTiles = L.tileLayer('https://api.maptiler.com/maps/toner-v2-lite/256/{z}/{x}/{y}@2x.png?key=' + accessToken, {
				"attribution": "<a href=\"https://www.maptiler.com/copyright/\" target=\"_blank\">&copy; MapTiler</a> <a href=\"https://www.openstreetmap.org/copyright\" target=\"_blank\">&copy; OpenStreetMap contributors</a>", 
			});
			_map.addLayer(mapboxTiles)
        }else if(tile == "mapnik"){
            L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
                maxZoom: 19,
                attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
            }).addTo(_map);
        }else{

            if(baseUrl.indexOf("qa.communecter") > -1 || baseUrl.indexOf("communecter-") > -1 || baseUrl.indexOf("communecter74-dev") > -1 || baseUrl.indexOf("dev.communecter") > -1 || baseUrl.indexOf("local") > -1 || baseUrl.indexOf("127.0.0.1") > -1){
                L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',{
                    attribution: 'Map tiles by <a href="http://stamen.com">Stamen Design</a>, under <a href="http://creativecommons.org/licenses/by/3.0">CC BY 3.0</a>. Data by <a href="http://openstreetmap.org">OpenStreetMap</a>, under <a href="http://www.openstreetmap.org/copyright">ODbL</a>'
                }).addTo(_map);
            }else{
                if($('script[src="https://api.mapbox.com/mapbox.js/v3.2.1/mapbox.js"]').length){
                    var accessToken = '2FAeSQOpVCo7WdsCmujj';
                    var mapboxTiles = L.tileLayer('https://api.maptiler.com/maps/eb5fb1d0-58c0-47ab-af0a-86ed434e011c/256/{z}/{x}/{y}@2x.png?key=' + accessToken, {
                        "attribution": "<a href=\"https://www.maptiler.com/copyright/\" target=\"_blank\">&copy; MapTiler</a> <a href=\"https://www.openstreetmap.org/copyright\" target=\"_blank\">&copy; OpenStreetMap contributors</a>"
                    });
                    _map.addLayer(mapboxTiles)
                }else{
                    var accessToken = '2FAeSQOpVCo7WdsCmujj';
    
                    var mapboxTiles = L.tileLayer('https://api.maptiler.com/maps/toner-v2/256/{z}/{x}/{y}@2x.png?key=' + accessToken, {
                        "attribution": "<a href=\"https://www.maptiler.com/copyright/\" target=\"_blank\">&copy; MapTiler</a> <a href=\"https://www.openstreetmap.org/copyright\" target=\"_blank\">&copy; OpenStreetMap contributors</a>",
                    });
                    _map.addLayer(mapboxTiles)
                }
            }
        }
    }

    this.addPolygon = function(data){
        var polygon = L.polygon(data).addTo(_map)
        _context.addPopUp(polygon)
    }

    this.fitBounds = function(){
        if(_options.geoJSONlayer && _options.geoJSONlayer.getLayers().length > 0){
            _map.fitBounds(_options.geoJSONlayer.getBounds())
            if(_map.getZoom() == 0){
                _map.setZoom(3)
            }
        }
        else if(_options.arrayBounds.length > 0){
            _map.fitBounds(_options.arrayBounds)
            if(_map.getZoom() == 0){
                _map.setZoom(3)
            }
        } 
        else{
            _map.panTo(_options.mapOpt.center);
            _map.setZoom(_options.mapOpt.zoom);
            _map.invalidateSize();
        }
    }
    /* public methodes ==========> */

    /* call init when constructing the object */
    init()
}