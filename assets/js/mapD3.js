/*global L, define */
// (function (window, document, undefined) {
//     'use strict';

    
// }(window, document));
/*global L */
(function (window, document, undefined) {
    'use strict';
        /**
            * SVG Icon
            * @type {L.SVGIcon}
            * @extends {L.Icon}
        */
        L.SVGIcon = L.Icon.extend({
            options: {
                /** @var {L.Point} */
                iconSize: [24, 24],
                /** @var {L.Point} */
                iconAnchor: [12, 12],
                /** @var {Function} */
                drawIcon: null,
                /** @var {string} */
                className: 'leaflet-svg-icon'
            },

            /**
             * @param {HTMLElement} icon
             * @returns {HTMLSVGElement}
             */
            createIcon: function (icon) {
                var size = L.point(this.options.iconSize);

                if (!icon || (icon.tagName != 'SVG')) {
                    icon = document.createElement('div');
                }

                // icon.width = size.x;
                // icon.height = size.y;

                this._setIconStyles(icon, 'svg');

                return icon;
            },

            /**
             * @param {HTMLElement} icon
             * @returns {null}
             */
            createShadow: function (icon) {
                return null;
            },

            /**
             * @param {HTMLElement} icon
             * @param {string} type
             * @private
             */
            _setIconStyles: function (icon, type) {
                if (typeof this.options.drawIcon == 'function') {
                    this.options.drawIcon.apply(this, arguments);
                }
                L.Icon.prototype._setIconStyles.apply(this, arguments);
                var size = L.point(this.options.iconSize);
                var anchor = size.divideBy(2);
                icon.style.marginLeft = (-anchor.x)+"px";
                icon.style.marginTop = (-size.y)+"px";
            }
        });

        /**
         * SVG Icon factory
         * @param {Object} options
         * @returns {L.SVGIcon}
         */
        L.svgIcon = function (options) {
            return new L.SVGIcon(options);
        };

        /**
         * AMD compatibility
         */
        if ((typeof define == 'function') && define.amd) {
            define(L.SVGIcon);
        }
    /**
     * Piechart Icon
     * @type {L.D3Icon}
     * @extends {L.SVGIcon}
     */
    L.D3Icon = L.SVGIcon.extend({
        options: {
            iconSize: [48, 48],
            iconAnchor: [24, 24],
            popupAnchor: [24, 24],
            className: 'leaflet-piechart-icon',
            type: 'pie'
        },

        /**
         * @param {HTMLSVGElement} icon
         * @param {string} type
         */
        _setIconStyles: function (icon, type) {
            var data = this.options.data;
            var label = this.options.label;
            var legendeActivated = this.options.legendeActivated;
            var container = typeof this.options.container !== 'undefined' ? this.options.container : "#graphD3";
            // set the color scale
            var color = d3.scaleOrdinal()
                .domain(this.options.legende)
                .range(d3.schemeTableau10);
            
            var Tooltip = $(container).find(".tooltip-wrapper");
    
            var mouseover = function(event, d) {
                d = typeof d.data !== "undefined" ? d.data : d;
                Tooltip
                    .css({
                        "opacity":1,
                        "z-index":9999,
                    "left": $(this).offset().left+ 'px',
                    "top": $(this).offset().top - $(container).offset().top + 'px'})
                Tooltip.find("p")
                    .html("Il y a "+d[1]+" organisation"+(d[1] > 1 ? "s" : "")+" de <span class='tooltip-label'>"+label+" "+d[0]+"</span>");
                d3.select(this)
                    .style("stroke", "black")
            }

            var mousemove = function(event, d) {
                    // .style("left", (d3.mouse(this)[0]+70) + "px")
                    // .style("top", (d3.mouse(this)[1]) + "px")
            }
            if ((type == 'svg') && data ) {
                data = Object.fromEntries(Object.entries(data).filter(([key]) => legendeActivated.includes(key)))
                if(this.options.type == 'donut') {

                    var total = this._getTotal(data);
                    var size = L.point(this.options.iconSize);
                    var center = size.divideBy(2);

                    // The radius of the pieplot is half the width or half the height (smallest one). I subtract a bit of margin.
                    var radius = Math.min(center.x, center.y) - 2;
                    // append the svg object to the div called 'my_dataviz'
                    var svg = d3.select(icon)
                        .attr('style', "position:absolute")
                    .append("svg")
                        .attr("width", size.x)
                        .attr("height", size.y)
                    .append("g")
                        .attr("transform", "translate(" + center.x+ "," + center.y+ ")");

                    // Compute the position of each group on the pie:
                    const pie = d3.pie()
                        .value(function(d) {return d[1]})
                    const data_ready = pie(Object.entries(data))
                    // Now I know that group A goes from 0 degrees to x degrees and so on.

                    // shape helper to build arcs:
                    var arcGenerator = d3.arc()
                        .innerRadius(radius * 0.5)
                        .outerRadius(radius)
                    // Build the pie chart: Basically, each part of the pie is a path that we build using the arc function.
                    

                    var mouseleave = function(event, d) {
                        Tooltip
                            .css({"opacity":0,"z-index": -999999})
                    }
                    svg
                        .selectAll('mySlices')
                        .data(data_ready)
                        .join('path')
                            .attr('d', arcGenerator)
                            .attr('fill', function(d){ return(color(d.data[0] == '' ? "Autre" : d.data[0])) })
                            .attr("stroke", "black")
                            .style('pointer-events', 'inherit')
                            .style("stroke-width", "2px")
                            .style("opacity", 0.75)
                            .on('mouseover', mouseover)                                                          // NEW
                            .on('mouseleave', mouseleave)
                }
                else if(this.options.type == 'pie') {

                    var total = this._getTotal(data);
                    var size = L.point(this.options.iconSize);
                    var center = size.divideBy(2);

                    // The radius of the pieplot is half the width or half the height (smallest one). I subtract a bit of margin.
                    var radius = Math.min(center.x, center.y) - 2;
                    // append the svg object to the div called 'my_dataviz'
                    var svg = d3.select(icon)
                        .attr('style', "position:absolute")
                        .append("svg")
                            .attr("width", size.x)
                            .attr("height", size.y)
                        .append("g")
                            .attr("transform", "translate(" + center.x+ "," + center.y+ ")");
                        

                    var mouseleave = function(event, d) {
                        Tooltip
                            .css({"opacity":0,"z-index": -999999})
                    }
                    // Compute the position of each group on the pie:
                    const pie = d3.pie()
                        .value(function(d) {return d[1]})
                    const data_ready = pie(Object.entries(data))
                    // Now I know that group A goes from 0 degrees to x degrees and so on.

                    // shape helper to build arcs:
                    var arcGenerator = d3.arc()
                        .innerRadius(0)
                        .outerRadius(radius)
                    
                    // Build the pie chart: Basically, each part of the pie is a path that we build using the arc function.
                    var path = svg
                        .selectAll('path')
                        .data(data_ready)
                        .join('path')
                            .attr('d', arcGenerator)
                            .attr('fill', function(d){ return(color(d.data[0] == '' ? "Autre" : d.data[0])) })
                            .attr("stroke", "black")
                            .style('pointer-events', 'inherit')
                            .style("stroke-width", "1px")
                            .style("opacity", 0.75)
                            // .on("mouseover", function (event, d) {
                            //     alert("data from event"+event.target)
                            // })
                        // .append("title")
                        //     .text(d => d.data[0]+" : " + d.data[1]);
                    path.on('mouseover', mouseover);                                                           // NEW
                    path.on('mouseleave', mouseleave);
                    svg
                        .selectAll('mySlices')
                        .data(data_ready)
                        .join('text')
                        .text(function(d){return (d.data[1]).toFixed(0)})
                        .attr("transform", function(d) { return `translate(${arcGenerator.centroid(d)})`})
                        .style("text-anchor", "middle")
                        .style("font-size", 10)
                        .style("color", function(d){ return(color(d.data[0] == '' ? "Autre" : d.data[0]))})
                        // .on("mouseover", mouseover)
                        // .on("mousemove", mousemove)
                        // .on("mouseleave", mouseleave)
                }
                else if(this.options.type == 'bar') {
                    var size = L.point(this.options.iconSize);
                    var center = size.divideBy(2);

                    // The radius of the pieplot is half the width or half the height (smallest one). I subtract a bit of margin.
                    var radius = Math.min(center.x, center.y) - 2;
                    // append the svg object to the div called 'my_dataviz'
                    var svg = d3.select(icon)
                        .attr('style', "position:absolute")
                        .append("svg")
                            .attr("width", size.x)
                            .attr("height", size.y)
                        .append("g")
                            .attr("transform",
                                "translate(-5,0)");


                    var mouseleave = function(event, d) {
                        Tooltip
                            .css({"opacity":0,"z-index": -999999})
                        d3.select(this)
                            .style("stroke", "none")
                    }
                        // Add Y axis
                    const x = d3.scaleBand()
                        .range([ 0, size.x ])
                        .domain(Object.keys(data))
                        .padding(0.2);
                    svg.append("g")
                        .attr("transform", `translate(0,${size.y})`)
                        .call(d3.axisBottom(x))
                        // Add Y axis
                    const y = d3.scaleLinear()
                        .domain([0, Math.max(...Object.values(data))])
                        .range([ size.y, 0]);
                    svg.append("g")
                        .attr("class", "myYaxis")
                        .call(d3.axisLeft(y));
                    var u = svg.selectAll("rect")
                        .data(Object.entries(data));
                    u
                        .join("rect")
                        .on("mouseover", mouseover)
                        .on("mousemove", mousemove)
                        .on("mouseleave", mouseleave)
                        .transition()
                        .duration(1000)
                        .attr("x", d => x(d[0]))
                        .attr("y", d => y(d[1]))
                        .attr("width", x.bandwidth())
                        .attr("height", d => size.y - y(d[1]))
                        .attr("fill", function(d){ return(color(d[0])) })
                }
            }

            L.SVGIcon.prototype._setIconStyles.apply(this, arguments);
        },

        /**
         * @param {Object[]} data Array of objects
         * @param {string} field Field to summarize
         * @returns {number}
         * @private
         */
        _getTotal: function (data) {
            var total = 0;
            for(const [key, value] of Object.entries(data)) {
                total += value;
            }
            return total;
        },
    });

    /**
     * Pie char Icon factory
     * @param {Object} options
     * @returns {L.D3Icon}
     */
    L.d3Icon = function (options) {
        return new L.D3Icon(options);
    };

    /**
     * Pie chart Marker
     * @type {L.Marker}
     */
    L.D3Marker = L.Marker.extend({
        options: {
            type: 'pie',
            icon: null,
            radius: 20,
            riseOnHover: true
        },

        initialize: function (latlng, options) {
            var opts = {};
            L.Util.extend(opts, options);
            if (opts.radius) {
                var diameter = opts.radius * 2;
                opts.iconSize = [diameter, diameter];
                opts.iconAnchor = [opts.radius, opts.radius];
            }
            opts.icon = L.d3Icon(opts);
            L.Marker.prototype.initialize.apply(this, [latlng, opts]);
        }
    });

    /**
     * Pie chart Marker factory
     * @param {L.LatLng} latlng
     * @param {Object} options
     * @returns {L.D3Marker}
     */
    L.d3Marker = function (latlng, options) {
        return new L.D3Marker(latlng, options);
    }
}(window, document));

var unique = function(array) {
    var a = array;
    for(var i=0; i<a.length; ++i) {
        for(var j=i+1; j<a.length; ++j) {
            if(a[i] === a[j])
                a.splice(j--, 1);
        }
    }

    return a;
};

function MapD3(options=null){
    /* private properties ==========> */
    var _context = this;
    var _map = null;
    var _options = {
        parentContainer: '#mapContainer',
        parentContainerName: 'mapContainer',
        container: '#mapContainerMap',
        hideContainer: 'search-content',
        data: {},
        colors: d3.scaleOrdinal()
                .range(d3.schemeTableau10),
        arrayBounds: [],
        bounds: null,
        legende: [],
        legendeActivated: [],
        lastLegende: [],
        legendeVar: "tags",
        legendeLabel: "type",
        clickedLegende: "",
        showLegende: true,
        dynamicLegende: true,
        groupBy: "type",
        markerType: 'default',
        markersCluster: null,
        geoJSONlayer: null,
        markerList: {},
        clusterType: 'pie',
        activeCluster: true,
        activePopUp: false,
        activePreview: false,
        listItem: true,
        distanceToMax: 0,
        elts:null,
        mapOpt:{
            dragging: true,
            draggable: false,
            center: [0, 0],
            centerOneZoom:18,
            zoom: 10,
            offset: {
                x: 0,
                y: 0
            },
            menuRight: null,
            btnHide: false,
            minZoom: 0,
            maxZoom: 18,
            mouseOver : false,
            doubleClick : false,
            onclickMarker: null,
        },
        mapCustom:{
            tile:"mapnik",
            css:[],
            icons: {
                pie: modules.map.assets + '/images/icons/pie.png',
                bar: modules.map.assets + '/images/icons/bar.png',
                donut: modules.map.assets + '/images/icons/donut.png',
                heatmap: modules.map.assets + '/images/icons/heatmap.png'
            },
            markers: {
                default: modules.map.assets + '/images/markers/citizen-marker-default.png',
                organization: modules.map.assets + '/images/markers/ngo-marker-default.png',
                classified: modules.map.assets + '/images/markers/classified-marker-default.png',
                proposal: modules.map.assets + '/images/markers/proposal-marker-default.png',
                poi: modules.map.assets + '/images/markers/poi-marker-default.png',
                project: modules.map.assets + '/images/markers/project-marker-default.png',
                event: modules.map.assets + '/images/markers/event-marker-default.png',
                answer: modules.map.assets + '/images/markers/services/tools-hardware.png',
                getMarker: function(data){
                    var markers = _options.mapCustom.markers;
                    var collectionGroups = {
                        organization: ["organization", "organizations", "NGO", "Cooperative"],
                        classified: ["classified", "classifieds"],
                        proposal: ["proposal", "proposals"],
                        poi: ["poi"],
                        project: ["project", "projects"],
                        event: ["event", "events"],
                        answer: ["answers"]
                    }
                    var marker = markers.default;

                    if(data){
                        if(data.profilMarkerImageUrl)
                            marker = data.profilMarkerImageUrl;
                        else if(data.collection){
                            $.each(collectionGroups, function(k, v){
                                if(v.indexOf(data.collection) > -1){
                                    marker = markers[k]
                                }
                            })
                        }
                    }

                    return marker;
                }
            },
            icon:{
                options:{
                    iconSize: [45, 55],
                    iconAnchor: [25, 45],
                    popupAnchor: [-3, -30]
                },
                getIcon:function(params){
                    if(_options.markerType == "default"){
                        var iconOptions = _options.mapCustom.icon.options;
                        iconOptions.data = params.elt;
                        iconOptions.iconUrl = _options.mapCustom.markers.getMarker(params.elt);
                        return L.icon(iconOptions);
                    }else{
                        var iconOptions = _options.mapCustom.icon.options;
                        iconOptions.data = params.elt;
                        iconOptions.type = _options.markerType;
                        iconOptions.legende = params.legende;
                        iconOptions.legendeActivated = params.legendeActivated;
                        iconOptions.label = _options.legendeLabel;
                        // iconOptions.iconUrl = _options.mapCustom.markers.getMarker(params.elt);
                        return L.d3Icon(iconOptions);
                    }
                }
            },
            lists: {
                text:"",
                createItemList: function(kElt, element, container){
                    if(element.type && $.inArray(element.type, _filtres.types) == -1){
                        _filtres.types.push(element.type)
                        var valueIdType = element.type.replace(/[^A-Za-z0-9]/g, "");
                        if (!$('#item_panel_map_' + valueIdType).length){
                            var newItem = "<button class='item_panel_map' id='item_panel_map_" + valueIdType + "'>" +
                                        "<i class='fa fa-tag' ></i> " + element.type +
                                        "</button>";
                            $(_options.parentContainer +' .panel_filter').append(newItem);

                            $("#item_panel_map_" + valueIdType).click(function(){
                                $(".item_panel_map").removeClass("active");
                                $("#item_panel_map_" + valueIdType).addClass("active");
                                _filtres.typesActived = []
                                if($.inArray(valueIdType, _filtres.typesActived) == -1){
                                    _filtres.typesActived.push(valueIdType)
                                }
                                _context.clearMap()
                                _context.addElts(_options.data)
                            })
                        }
                    }
                    //prépare le nom (ou "anonyme")
                    var name = (element.name)?element.name:(element.title?element.title:"Anonyme")
                    //recuperation de l'image de profil (ou image par defaut)
                    var imgProfilPath = _options.mapCustom.getThumbProfil(element)
                    var button = '<div class="element-right-list" id="element-right-list-' + kElt + '">' +
                                '<button class="item_map_list item_map_list_' + kElt + '">' +
                                "<div class='left-col'>" +
                                "<div class='thumbnail-profil'><img class='lzy_img' height=50 width=50 src='" + modules.co2.url +"/images/thumbnail-default.jpg' data-src='" + imgProfilPath + "'></div>" +
                                "</div>" +
                                "<div class='right-col'>" +
                                "<div class='info_item pseudo_item_map_list'>" + name + "</div>";
                    if(element.tags){
                        button += "<div class='info_item items_map_list'>";
                        var totalTags = 0;
                        $.each(element.tags, function(index, value){
                            totalTags++;
                            if(totalTags < 4){
                                var t = tradCategory[value]?tradCategory[value]:value;
                                button += "<a href='javascript:' class='tag_item_map_list'>#" + t + " </a>";
                            }
                            var valueId = value.replace(/[^A-Za-z0-9]/g, "");
                            //si l'item n'existe pas deja
                            if (!$('#item_panel_map_' + valueId).length){
                                var newItem = "<button class='item_panel_map' id='item_panel_map_" + valueId + "'>" +
								                "<i class='fa fa-tag' ></i> " + value +
                                            "</button>";
                                $(_options.parentContainer + ' .panel_map2').append(newItem);

                                $("#item_panel_map_"+valueId).click(function(){
                                    $(".item_panel_map").removeClass("active");
                                    $("#item_panel_map_" + valueId).addClass("active");
                                    
                                    _filtres.tagsActived = [];
                                    if($.inArray(value, _filtres.tagsActived) == -1){
                                        _filtres.tagsActived.push(value)
                                    }
                                    _context.clearMap()
                                    _context.addElts(_options.data)
                                })
                            }
                            if($.inArray(value, _filtres.tags) == -1)
                                _filtres.tags.push(value)
                        })
                        button += "</div>";
                    }

                    if(element.address){
                        button += "<div class='info_item city_item_map_list inline'>";
                        if(element.address.addressLocality){
                            button += element.address.addressLocality + " "
                        }
                        if(element.address.postalCode){
                            button += element.address.postalCode + " "
                        }
                        if(element.address.addressCountry){
                            button += element.address.addressCountry + " "
                        }
                        button += "</div>";
                    }
                    button += "</div>";

                    if(element.type == "event" || element.type == "events"){
                        button += displayStartAndEndDate(element);
                    }
                    button += '<div class="col-xs-12 separation"></div>' +
                                '</button>' +
                            '</div>';
                    $(_options.parentContainer + " .liste_map_element").append(button)
                
                    $(_options.parentContainer + " .item_map_list_" + kElt).click(function(){
                        if(element.geo && element.geo.latitude && element.geo.longitude){
                            var m = _options.markerList[kElt]
                            _options.markersCluster.zoomToShowLayer(m, function(){
                                m.openPopup()
                                coInterface.bindLBHLinks();
                            })
                        }else{
                            $(_options.parentContainer + " .modalItemNotLocated").modal('show');
                            $(_options.parentContainer + " .modalItemNotLocated .modal-body").html("<i class='fa fa-spin fa-reload'></i>");
                            $(_options.parentContainer + " .modalItemNotLocated .modal-body").html(_options.mapCustom.getPopup(element))
                            $(_options.parentContainer + " .modalItemNotLocated #btn-open-details").click(function(){
                                $("#popup" + kElt).click();
                                if(networkJson && networkJson.dataSrc){
                                    urlCtrl.loadByHash("'+url+'");
                                }
                            })

                            $(_options.parentContainer + " .modalItemNotLocated #popup" + kElt).click(function(){
                                $(_options.parentContainer + " .modalItemNotLocated").modal('hide')
                            })
                        }
                    })
                }
            },
            transformData: function(allData, type) {
                if(_options.dynamicLegende == "false"  || _options.dynamicLegende == false){
                    if(type.data){
                        for (const [key, legende] of Object.entries(_options.legende)) {
                            if(type[_options.legendeVar].includes(legende)){
                                allData[legende] = allData[legende] || 0;
                                allData[legende] += type.data[legende];
                            }
                        }
                    }else{
                        var existe = false;
                        if(typeof type[_options.legendeVar] != "undefined"){
                            if(type[_options.legendeVar] instanceof Array || type[_options.legendeVar] instanceof Object){
                                for (const [key, legende] of Object.entries(_options.legende)) {
                                    if(type[_options.legendeVar].includes(legende) && legende != "Autre"){
                                        allData[legende] = allData[legende] || 0;
                                        allData[legende] += 1;
                                        existe = true;
                                    }
                                    if(key == _options.legende.length - 1){
                                        if(!existe){
                                            allData["Autre"] = allData["Autre"] || 0;
                                            allData["Autre"] += 1;
                                        }
                                    }
                                }
                            }
                            
                        }else{
                            allData["Autre"] = allData["Autre"] || 0;
                            allData["Autre"] += 1;
                        }
                    }
                }else if(type.data){
                    for (const [key, value] of Object.entries(type.data)) {
                        allData[key] = allData[key] || 0;
                        allData[key] += value;
                    }
                }else {
                    if(type[_options.groupBy] && type[_options.groupBy] != ""){
                        allData[type[_options.groupBy]] = allData[type[_options.groupBy]] || 0;
                        allData[type[_options.groupBy]] += 1;
                    }else{
                        allData["Autre"] = allData["Autre"] || 0;
                        allData["Autre"] += 1;
                    }
                }
                return allData;
            },
            getData: function(allData, cluster){
                if(cluster._markers){
                    cluster._markers.forEach(element => {
                        if(_options.mapCustom.transformDataCustom){
                            allData = _options.mapCustom.transformDataCustom(allData, element.options.icon.options.data, _context)
                        }else{
                            allData = _options.mapCustom.transformData(allData, element.options.icon.options.data)
                        }
                    });
        
                }
                if(cluster._childClusters.length > 0){
                    cluster._childClusters.forEach(clusterChild => {
                        _options.mapCustom.getData(allData, clusterChild);
                    })
                }
            },
            getClusterIcon: function(cluster){
                var lastdata = [];
                var width = 76;
                _options.mapCustom.getData(lastdata,cluster);
                if(_options.clickedLegende.length < 1){
                    return L.d3Icon({type: _options.clusterType, legende: _options.legende,label: _options.legendeLabel, legendeActivated: _options.legendeActivated, data: lastdata, iconSize: [width, width]});
                }else{
                    if(typeof paramsMapCO.mapCustom.getClusterIcon(cluster) != "undefined")
                        return paramsMapCO.mapCustom.getClusterIcon(cluster);
                    var childCount = cluster.getChildCount();
                    var c = ' marker-cluster-';
                    if (childCount < 10) {
                        c += 'small';
                    } else if (childCount < 100) {
                        c += 'medium';
                    } else {
                        c += 'large';
                    }
                    return L.divIcon({ html: '<div>' + childCount + '</div>', className: 'marker-cluster' + c, iconSize: new L.Point(40, 40) });
                    
                }
            },
            getThumbProfil: function (data) {
                var imgProfilPath = modules.map.assets + "/images/thumb/default.png";
                if (typeof data.profilThumbImageUrl !== "undefined" && data.profilThumbImageUrl != "")
                    imgProfilPath = baseUrl + data.profilThumbImageUrl;
                else
                    imgProfilPath = modules.map.assets + "/images/thumb/default_" + data.collection + ".png";
    
                return imgProfilPath;
            },
            getPopup: function(data){
                var id = data._id ? data._id.$id:data.id;
                var imgProfil = _options.mapCustom.getThumbProfil(data)

                var eltName = data.title ? data.title: ((typeof data.properties != "undefined" && typeof data.properties.name != "undefined") ? data.properties.name : data.name);
                var popup = "";
                popup += "<div class='padding-5' id='popup" + id + "'>";
                popup += "<img src='" + imgProfil + "' height='30' width='30' class='' style='display: inline; vertical-align: middle; border-radius:100%;'>";
                popup += "<span style='margin-left : 5px; font-size:18px'>" + eltName + "</span>";

                if(data.tags && data.tags.length > 0){
				    popup += "<div style='margin-top : 5px;'>";
                    var totalTags = 0;
                    $.each(data.tags, function(index, value){
                        totalTags++;
                        if (totalTags < 3) {
                            popup += "<div class='popup-tags'>#" + value + " </div>";
                        }
                    })
                    popup += "</div>";
                }
                if(data.address){
                    var addressStr="";
                    if(data.address.streetAddress)
                        addressStr += data.address.streetAddress;
                    if(data.address.postalCode)
                        addressStr += ((addressStr != "")?", ":"") + data.address.postalCode;
                    if(data.address.addressLocality)
                        addressStr += ((addressStr != "")?", ":"") + data.address.addressLocality;
                    popup += "<div class='popup-address text-dark'>";
                    popup += 	"<i class='fa fa-map-marker'></i> "+addressStr;
                    popup += "</div>";
                }
                if(data.shortDescription && data.shortDescription != ""){
                    popup += "<div class='popup-section'>";
					popup += "<div class='popup-subtitle'>Description</div>";
					popup += "<div class='popup-shortDescription'>" + data.shortDescription + "</div>";
				    popup += "</div>";
                }
                if((data.url && typeof data.url == "string") || data.email || data.telephone){
                    popup += "<div id='pop-contacts' class='popup-section'>";
                    popup += "<div class='popup-subtitle'>Contacts</div>";
                    
                    if(data.url && typeof data.url === "string"){
                        popup += "<div class='popup-info-profil'>";
                        popup += "<i class='fa fa fa-desktop fa_url'></i> ";
                        popup += "<a href='" + data.url + "' target='_blank'>" + data.url + "</a>";
                        popup += "</div>";
                    }

                    if(data.email){
                        popup += "<div class='popup-info-profil'>";
                        popup += "<i class='fa fa-envelope fa_email'></i> " + data.email;
                        popup += "</div>";
                    }

                    if(data.telephone){
                        popup += "<div class='popup-info-profil'>";
                        popup += "<i class='fa fa-phone fa_phone'></i> ";
                        var tel = ["fixe", "mobile"];
                        var iT = 0;
                        $.each(tel, function(keyT, valT){
                            if(data.telephone[valT]){
                                $.each(data.telephone[valT], function(keyN, valN){
                                    if(iT > 0)
                                        popup += ", ";
                                    popup += valN;
                                    iT++; 
                                })
                            }
                        })
                        popup += "</div>";
                    }

                    popup += "</div>";
				    popup += "</div>";
                }
                if(data.collection && id){
                    var url = '#page.type.' + data.collection + '.id.' + id;
                    popup += "<div class='popup-section'>";
                    if(_options.activePreview)
                        popup += "<a href='" + url + "' class='lbh-preview-element item_map_list popup-marker' id='popup" + id + "'>";
                    else
                        popup += "<a href='" + url + "' target='_blank' class='lbh item_map_list popup-marker' id='popup" + id + "'>";
                    popup += '<div class="btn btn-sm btn-more col-md-12">';
                    popup += '<i class="fa fa-hand-pointer-o"></i>' + trad.knowmore;
                    popup += '</div></a>';
                }
                popup += '</div>';
                popup += '</div>';

                return popup;
            },
            addMarker:null,
            addElts:null,
            transformDataCustom : null
        },
        buttons:{
            fullScreen:{
                allow:false,
                position: "bottomLeft",
                icon:{
                    maximize: 'expand',
                    minimize: 'compress'
                },
                style:{
                    default:{
                        border: "none",
                        borderRadius: "100%",
                        width: "40px",
                        height: "40px",
                        background: "rgba(255,255,255,.5)",
                        outline:"none"
                    },
                    onHover:{
                        background: "rgba(255,255,255,1)"
                    }
                }
            }
        },
        actionButtons:[]
    }
    var _utils = {
        bindMap: function(){
            $(_options.parentContainer + " .input_name_filter").keyup(function(){
                if (notNull(_filtres.timeoutAddCity))
                    clearTimeout(_filtres.timeoutAddCity);
                
                _filtres.timeoutAddCity = setTimeout(function () {
					_filtres.search = $(_options.parentContainer + " .input_name_filter").val();
                    _context.clearMap();
                    _context.addElts(_options.data);
				}, 500);
            })

            $(_options.parentContainer + " .item_panel_filter_all").off().click(function(){
                $(_options.parentContainer + " .item_panel_map").removeClass("active");
                $(_options.parentContainer + " .item_panel_filter_all").addClass("active");
                _filtres.typesActived = [];
                _context.clearMap();
                _context.addElts(_options.data)
            })

            $(_options.parentContainer + " .item_panel_map_all").off().click(function(){
                _filtres.tagsActived = [];
                $(_options.parentContainer  + " .item_panel_map").removeClass("active")
                $(_options.parentContainer + " .item_panel_map_all").addClass("active")
                _context.clearMap();
                _context.addElts(_options.data)
            })

            $(_options.parentContainer + " .btn-hide-map").off().click(function(){
                showMap(false);
            })

            $(_options.parentContainer + " #btn-close-menuRight").off().click(function(){
                if(!$(this).data("close")){
                    $("#menuRight"+ _options.parentContainerName).hide();
                    $(this).html('<i class="fa fa-angle-left"></i>');
                    $(_options.parentContainer + " .closeMenuRight").css('right', "0px");
                    $(this).data("close", true);
                }else{
                    $("#menuRight"+ _options.parentContainerName).show();
                    $(this).html('<i class="fa fa-angle-right"></i>');
                    var right = "341px";
                    if(_options.mapOpt.menuRight && _options.mapOpt.menuRight.btnClose && _options.mapOpt.btnClose.right){
                        right = _options.mapOpt.btnClose.right;
                    }
                    $(_options.parentContainer + " .closeMenuRight").css('right', right);
                    $(this).data("close", false);
                }
            })
        },
        generateRandomId: function(){
            return Math.random().toString(36).replace(/[^a-z]+/g, '').substr(2, 10);
        }
    }
    var _filtres = {
        types: [],
        typesActived: [],
        tags: [],
        tagsActived: [],
        search: "",
        result: function(elt){
            var res = _filtres.isTags(elt) && _filtres.isSearch(elt) && _filtres.isType(elt)
            return res;
        },
        isTags: function(elt){
            var res = false;
            if(Object.keys(_filtres.tagsActived).length == 0 || _filtres.tagsActived || _filtres.tagsActived.length == 0){
                res = true;
            }else{
                $.each(_filtres.tagsActived, function(k, v){
                    if(elt.tags && $.inArray(v, elt.tags) > -1)
                        res = true;
                })
            }
            return res;
        },
        isType: function(elt){
            var res = false;
            if(Object.keys(_filtres.typesActived).length == 0 || _filtres.typesActived || _filtres.typesActived.length == 0){
                res = true
            }else{
                $.each(_filtres.typesActived, function(k, v){
                    if(elt.type && v == elt.type)
                        res = true;
                })
            }
            return res;
        },
        isSearch: function(elt){
            var res = false;
            if(_filtres.search){
                if(elt.name && elt.name.search(new RegExp(_filtres.search, "i")) >= 0)
                    res = true;
            }else{
                res = true
            }
            return res;
        }
    }
    /* <========== private properties */

    /* private methodes ==========> */
    var initVar = function(){
        if(options){
            _options = $.extend(true, {}, _options, options)

            _options.parentContainer = (options.container) ? options.container:"#mapContainer";
            _options.parentContainerName = _options.parentContainer.replace("#", "").replace(".","")
            _options.container = _options.parentContainer + "Map";
            _options.geoJSONlayer = new L.LayerGroup();
            if(_options.activeCluster){
                _options.markersCluster = new L.markerClusterGroup({
                    iconCreateFunction: function (cluster) {
                        return _options.mapCustom.getClusterIcon(cluster);
                    }
                })
            }
        }
    }

    var initView = function(){
        var view = "";

        view += "<div class='mapLoading'></div>";
        view += ` <div class="tooltip-wrapper">
            <div class="tooltip-content">
                <p></p>
            </div>
        </div>`;
        if(_options.mapOpt.menuRight){
            var dataCloseMenuRight = false,
                sizeList = $(_options.parentContainer).height() - 80;

            if(_options.mapOpt.menuRight.btnClose){
                view += '<div class="closeMenuRight">'+
                            '<button type="button" data-close="'+dataCloseMenuRight+'" class="btn btn-default " id="btn-close-menuRight" data-toggle="dropdown">' +
                                '<i class="fa fa-angle-right"></i>' +
                            '</button>' +
                        '</div>';
            }

            view += '<div id="menuRight' + _options.parentContainerName + '" class="hidden-xs col-sm-3 no-padding menuRight" style="">' +
					'<div id="menuRight_search' + _options.parentContainerName + '" class="col-sm-12 no-padding menuRight_search">' +
					'<div class="menuRight_header col-sm-12 padding-10">' +
					'<div class="col-xs-6">' +
					'<span id="result' + _options.parentContainerName + '" class=""></span>' +
					'<span class="menuRight_header_title"> '+tradMap.Results+' </span>' +
                '</div>';
            
            if(_options.mapOpt.btnHide){
                view += '<div class="col-xs-2 pull-right">' +
					'<button type="button" class="btn btnHeader dropdown-toggle btn-hide-map" data-toggle="dropdown">' +
					'<i class="fa fa-times"></i>' +
					'</button>' +
					'</div>';
            }
            view += '<div class="col-xs-2 pull-right">' +
				'<button type="button" class="btn btnHeader dropdown-toggle" id="btn-filters' + _options.parentContainerName + '" data-toggle="dropdown">' +
				'<i class="fa fa-filter"></i>' +
				'</button>' +
				'<ul class="dropdown-menu panel_filter panel_map"  role="menu" aria-labelledby="panel_filter" style="overflow-y: auto; max-height: 400px;">' +
				'<h3 class="title_panel_map"><i class="fa fa-angle-down"></i> '+tradMap['Filter results by types']+'</h3>' +
				'<button class="item_panel_map active item_panel_filter_all" >' +
				'<i class="fa fa-star"></i> All' +
				'</button>' +
				'</ul>' +
				'</div>' +
				'<div class="col-xs-2 pull-right">' +
				'<button type="button" class="btn btnHeader dropdown-toggle" id="btn-panel' + _options.parentContainerName + '" data-toggle="dropdown">' +
				'<i class="fa fa-tags"></i>' +
				'</button>' +
				'<ul class="dropdown-menu panel_map panel_map2 pull-right" role="menu" aria-labelledby="panel_map" style="overflow-y: auto; max-height: 400px;">' +
				'<h3 class="title_panel_map"><i class="fa fa-angle-down"></i> '+tradMap['Filter results by tags']+'</h3>' +
				'<button class="item_panel_map active item_panel_map_all" >' +
				'<i class="fa fa-star"></i> All' +
				'</button>' +
				'</ul>' +
				'</div>' +
				'</div>' +


				'<div id="menuRight_search_filters' + _options.parentContainerName + '" class="col-sm-12 no-padding menuRight_search_filters">' +
				'<input class="form-control date-range active input_name_filter" type="text" placeholder="'+tradMap['Filter by names']+'">' +
				'</div>' +
				'<div class="menuRight_body col-sm-12 no-padding liste_map_element" ' +
				' style="height: ' + sizeList + 'px;"></div>' +
				'</div>' +
                '</div>';
            
            view += '<div class="modal fade modalItemNotLocated" role="dialog">' +
				'<div class="modal-dialog">' +
				'<div class="modal-content" style="top: 125px;">' +
				'<div class="modal-header">' +
				'<button type="button" class="close" data-dismiss="modal">&times;</button>' +
				'<h4 class="modal-title text-dark">' +
				'<i class="fa fa-map-marker"></i>' + trad["This data is not geolocated"] +
				'</h4>' +
				'</div>' +
				'<div class="modal-body"></div>' +
				'<div class="modal-footer" style="border-top: none;">' +
				'<button type="button" class="btn btn-default btn-sm btn-danger" data-dismiss="modal"><i class="fa fa-times"></i>' + trad.closed + '</button>' +
				'</div>' +
				'</div>' +
				'</div>' +
				'</div>';
        }else{
            if (_options.mapOpt.btnHide) {
				view += '<div class="col-xs-2 div-btn-hide">' +
					'<button type="button" class="btn btnHeader dropdown-toggle btn-hide-map" id="" data-toggle="dropdown" style="background-color: #e6434d; color: white;">' +
					'<i class="fa fa-times"></i>' +
					'</button>' +
					'</div>';
			}
        }
        // view += '<div id="' + _options.container + '" style="z-index: 1; width: 100%; position: fixed; bottom:0; top:inherit;"></div>';
        view += '<div id="' + _options.container + '" style="z-index: 1; width: 100%; height: 100%; position:relative;"></div>';

        $(_options.parentContainer).html(view);

        if(_options.mapOpt.menuRight && _options.mapOpt.menuRight.close){
            $("#menuRight"+ _options.parentContainerName).hide();
            $(_options.parentContainer + " #btn-close-menuRight").html('<i class="fa fa-angle-left"></i>');
            $(_options.parentContainer + " .closeMenuRight").css('right', "0px");
            $(_options.parentContainer + " #btn-close-menuRight").data("close", true);
        }

        if(_options.buttons.fullScreen.allow)
            initBtnFullScreen();
        
        if(_options.actionButtons && Array.isArray(_options.actionButtons) && _options.actionButtons.length > 0){
            initBtnActions()
        }
    }

    var initBtnFullScreen = function(){
        var optBtnFullScreen = _options.buttons.fullScreen,
            fullScreenActived = false,
            $parentContainer = $(_options.parentContainer),
            buttonContainerId = _options.parentContainerName + "-btn-fullscreen-container",
            defaultSizeParentContainer = {
                width: $parentContainer.width(),
                height: $parentContainer.height()
            }

        var btnContainerPositionOptions = {
            topLeft: { left:0, top:0 },
            bottomLeft: { left:0, bottom:0 },
            topRight: { top:0, right:0 },
            bottomRight: { bottom:0, right:0 },
            default: { left:0, bottom:0 }
        }
        var btnContainerPosition = (btnContainerPositionOptions[optBtnFullScreen.position])? 
                                    btnContainerPositionOptions[optBtnFullScreen.position]:
                                    btnContainerPositionOptions.default;
        var btnContainerStyle = {
            position:"absolute",
            zIndex: 100000,
            padding:"20px"
        }
        $.each(btnContainerPosition, function(property, value){
            btnContainerStyle[property] = value;
        })

        var button = `
            <div id="${buttonContainerId}">
                <button><i class="fa fa-${optBtnFullScreen.icon.maximize}" aria-hidden="true"></i></button>
            </div>
        `
        $parentContainer.append(button)

        var $buttonContainerId = $("#"+buttonContainerId),
            $button = $buttonContainerId.find("button");
        
        $buttonContainerId.css(btnContainerStyle)
        $button.css(optBtnFullScreen.style.default)
        $button.hover(function(){
            $button.css(optBtnFullScreen.style.onHover)
        }, function(){
            $button.css(optBtnFullScreen.style.default)
        })

        $button.click(function(){
            if(fullScreenActived){
                $parentContainer.css({
                    position:"relative",
                    width:defaultSizeParentContainer.width,
                    height:defaultSizeParentContainer.height
                })
                $("html, body").css({
                    overflow:"visible"
                });
                $button.html(`<i class="fa fa-${optBtnFullScreen.icon.maximize}" aria-hidden="true"></i>`)
                $("html, body").scrollTop($parentContainer.offset().top - 100)
            }else{
                var mainNavHeight = document.getElementById("mainNav").offsetHeight
                $parentContainer.css({
                    position:"absolute",
                    height:`calc(100vh - ${mainNavHeight}px)`,
                    width:"100vw",
                    top:0,
                    left:0,
                    zIndex:999
                })
                $("html, body").scrollTop(0).css({
                    overflow:"hidden"
                });
                $button.html(`<i class="fa fa-${optBtnFullScreen.icon.minimize}" aria-hidden="true"></i>`)
            }

            setTimeout(function(){
                _map.invalidateSize()
            }, 400)

            fullScreenActived = !fullScreenActived;
        })
    }

    var initBtnActions = function(){
        var btnActionItems = "";
        for(let i in _options.actionButtons){
            _options.actionButtons[i].id = `btn-map-action_${_utils.generateRandomId()}`;
            btnActionItems += `<li id="${_options.actionButtons[i].id}"><i class="fa fa-${_options.actionButtons[i].icon}" aria-hidden="true"></i></li>`
        }

        var btnActions = `
            <div class="btn-map-actions-container">
                <ul class="btn-map-actions">
                    ${btnActionItems}
                </ul>
                <button id="btn-toggle-map-actions"><i class="fa fa-plus" aria-hidden="true"></i></button>
            </div>
        `
        $(_options.parentContainer).append(btnActions);

        $('#btn-toggle-map-actions').click(function(){
            $(this).toggleClass('active')
            $(".btn-map-actions").toggleClass('active')
        })
        for(let i in _options.actionButtons){
            $("#"+ _options.actionButtons[i].id).click(function(){
                if(_options.actionButtons[i].onclick){
                    _options.actionButtons[i].onclick()
                }
                $('#btn-toggle-map-actions').removeClass("active")
                $(".btn-map-actions").removeClass('active')
            })
        }
    }

    var init = function(){
        if($(options.container).length > 0){
            initVar()
            initView()
            _context.showLoader();
            _map = L.map(_options.container, _options.mapOpt);
            _context.setTile(_options.mapCustom.tile)
            L.control.zoom({
                position: 'topleft',
            }).addTo(_map);

            if(_options.elts){
                _context.addElts(_options.elts)
            }else{
                setTimeout(function(){
                    _map.invalidateSize()
                }, 400)
            }
        }
    }
    /* <========== private methodes */

    /* getters & setters ==========> */
    this.getMap = function(){
        return _map;
    }
    this.getOptions = function(){
        return _options;
    }
    this.setOptions = function(newOptions){
        _options = newOptions;
    }
    this.getUtils = function(){
        return _utils;
    }
    this.getFiltres = function(){
        return _filtres;
    }
    /* <========== getters & setters */

    /* public methodes ==========> */
    this.addElts = function (data, refresh) {
        _context.clearMap()
        if(_options.mapCustom.addElts){
            _options.mapCustom.addElts(_context, data)
        }else{
            _options.legende.push("Autre");
            _options.legende = unique(_options.legende);
            if(_options.markerType == "geoshape"){
                var itteration = 1;
                data = data.features ? data.features : data;
                $.each(data, function(k, v){
                    var geoV = {};
                    if(v.type && v.geometry){
                        geoV = v;
                    }else if(v.geoShape){
                        geoV = {
                            type: "Feature",
                            properties: {
                                name: v.name
                            },
                            geometry: v.geoShape
                        }
                    }
                    _context.addGeoJSON(geoV);
                    if(itteration == Object.keys(data).length){
                        var geoJson = L.geoJSON(_options.arrayBounds, {
                            onEachFeature: function(feature, layer){
                                layer.on("mouseover", function(){
                                    geoJson.setStyle({color: 'gray', opacity: 0.5});
                                    this.setStyle({
                                        color: '#0000FF',
                                        opacity: 1
                                    })
                                })
                                layer.on("mouseout", function(){
                                    geoJson.setStyle({color: '#0000FF', opacity: 0.5});
                                })
                                _context.addPopUp(layer, feature.properties);
                            },
                            style: {
                                color: "#0000FF"
                            }
                        });
                        _options.geoJSONlayer.addLayer(geoJson);
                        _map.addLayer(geoJson);
                        _context.hideLoader();
                        _map.invalidateSize();
                        _map.fitBounds(geoJson.getBounds())
                    }
                    itteration++;
                });
            }else if(_options.markerType == "heatmap"){
                if(typeof refresh == 'undefined' || refresh){
                    _options.clickedLegende = "";
                    _options.data = data;
                }
                var cfg = {
                    // radius should be small ONLY if scaleRadius is true (or small radius is intended)
                    // if scaleRadius is false it will be the constant radius used in pixels
                    "radius": 20,
                    "maxOpacity": .75,
                    // scales the radius based on map zoom
                    "scaleRadius": false,
                    // if set to false the heatmap uses the global maximum for colorization
                    // if activated: uses the data maximum within the current map boundaries
                    //   (there will always be a red spot with useLocalExtremas true)
                    "useLocalExtrema": true,
                    // which field name in your data represents the latitude - default "lat"
                    latField: 'lat',
                    // which field name in your data represents the longitude - default "lng"
                    lngField: 'lng',
                    // which field name in your data represents the data value - default "value"
                    valueField: 'count',
                    gradient: {
                        0.25: "rgb(0,0,255)",
                        0.55: "rgb(3 173 3)",
                        0.85: "#f9f952",
                        1.0: '#ff2b40'
                    }
                };
                var heatMapData = {max: 2000, data: []};
                var heatmapLayer = new HeatmapOverlay(cfg);
                var geoJson = [];
                var i = 1;
                var geoJsonLayer = null;
                $.each(data, function(k, v){
                    if(typeof v.geoShape != "undefined"){
                        _context.addGeoJSON({
                            type: "Feature",
                            properties: {
                                name: v.name
                            },
                            geometry: v.geoShape
                        });
                    }
                    if(_filtres.result(v) && typeof v.geoShape == "undefined" && typeof v.geo != "undefined"){
                        heatMapData.data.push({lat: v.geo.latitude, lng: v.geo.longitude, count: 1});
                        var latLon = [v.geo.latitude, v.geo.longitude];
                        _options.arrayBounds.push(latLon)
                    }
                    if(i == Object.keys(data).length){
                        geoJsonLayer = L.geoJSON(_options.arrayBounds, {
                            onEachFeature: function(feature, layer){
                                layer.on("mouseover", function(){
                                    geoJsonLayer.setStyle({color: 'gray', opacity: 0.5});
                                    this.setStyle({
                                        color: '#0000FF',
                                        opacity: 1
                                    })
                                })
                                layer.on("mouseout", function(){
                                    geoJsonLayer.setStyle({color: '#0000FF', opacity: 0.5});
                                })
                                _context.addPopUp(layer, feature.properties);
                            },
                            style: {
                                color: "#0000FF"
                            }
                        });
                        heatmapLayer.setData(heatMapData);
                        _options.geoJSONlayer.addLayer(geoJsonLayer);
                        _options.geoJSONlayer.addLayer(heatmapLayer);
                        _map.addLayer(geoJsonLayer);
                        _map.addLayer(heatmapLayer);
                    }
                    i++;
                })
                _context.hideLoader();
                _map.invalidateSize();
                _context.fitBounds(data);
            }else{
                if(typeof refresh == 'undefined' || refresh){
                    _options.clickedLegende = "";
                    _options.data = data;
                }
                var geoJson = [];
                $.each(data, function(k, v){
                    if(!_options.data[k])
                        _options.data[k] = v;
                    if(_options.dynamicLegende == true || _options.dynamicLegende == 'true'){
                        if(v.data){
                            _options.legende = unique(_options.legende.concat(Object.keys(v.data)))
                        }else if(v[_options.groupBy] instanceof Array || v[_options.groupBy] instanceof Object){
                            _options.legende = unique(_options.legende.concat(Object.keys(v[_options.groupBy])))
                        }else{
                            _options.legende.push((!v[_options.groupBy] || v[_options.groupBy] == "" ? "Autre" : v[_options.groupBy]));
                            _options.legende = unique(_options.legende);
                        }
                    }
                    if(typeof v.geoShape != "undefined"){
                        geoJson.push({
                                type: "Feature",
                                properties: {
                                    name: v.name,
                                    id: k
                                },
                                geometry: v.geoShape
                        });
                    }
                    if(_filtres.result(v) && typeof v.geoShape == "undefined" && typeof v.geo != "undefined"){
                        if(typeof refresh == "undefined" || refresh){
                            _options.legendeActivated = unique(_options.legendeActivated.concat(_options.legende));
                        }
                        v.id = k;
                        _context.addMarker({ elt: v, legende: _options.legende, legendeActivated: _options.legendeActivated })
                        _options.mapCustom.lists.createItemList(k, v, _options.container)
                    }
                })
                _options.geoJSONlayer.addLayer(L.geoJSON(geoJson, {
                    onEachFeature: function(feature, layer){
                        _context.addPopUp(layer, feature);
                        if(_options.mapOpt.onclickMarker){
                            layer.on('click', function(){
                                _options.mapOpt.onclickMarker(feature);
                            });
                        }
                    },
                    style: {
                        color: "#0000FF"
                    }
                }));
                _map.addLayer(_options.geoJSONlayer);
                if(_options.activeCluster)
                    _map.addLayer(_options.markersCluster)
                _utils.bindMap();
        
                if(_options.forced){
                    if(_options.forced.latLon && _options.forced.zoom){
                        _map.setView(_options.forced.latLon, _options.forced.zoom)
                    }else{
                        if(_options.forced.latLon)
                            _map.panTo(_options.forced.latLon)
                        if(_options.forced.zoom)
                            _map.setZoom(_options.forced.zoom)
                    }
                }
                if(_options.showLegende){
                    // if(document.querySelectorAll("#info-pane").length < 1){
                    //     $("[id="+_options.container+"]").append('<div id="info-pane" class="leaflet-bar chart-container" style="position: absolute;right: 20px;top: 0;z-index: 9999999;background: white;display: none; ">'+
                    //         '<div style="max-height: 500px;overflow-y: auto;"><svg id="chartCanvas"></svg></div>'+
                    //         '<button id="chartBtn" class="btn btn-sm">Enregistrer</button>'+
                    //     '</div>');
                    // }
                    if(document.querySelectorAll(".chart-legende-container").length < 1){
                        legende = L.control({
                            position: "topright"
                        });
                        legende.onAdd = function(map){
                            var div = L.DomUtil.create('div', 'chart-legende-container');
                            var str = '<div style="max-height: 500px;overflow-y: auto;"><svg id="chartCanvas"></svg></div>';
    
                            str += '<button id="chartBtn" class="btn btn-sm">Enregistrer</button>';
                            div.innerHTML = str;
                            return div;
                        }
                        legende.addTo(_map)
                    }
                    var Svg = d3.select("#chartCanvas")
                        .attr("width", 200)
                        .attr("height", _options.legende.length * 20 + 20);
                    Svg.selectAll(".d3map-legend").remove();
                    _options.lastLegende = JSON.parse(JSON.stringify(_options.legendeActivated));
                    // Add one dot in the legend for each name.
                    var group = Svg.selectAll("mydots")
                    .data(_options.legende)
                    .join((enter) => {
                        let g = enter;
                        g = g.append("g").attr("height", "20px").attr("width", "100%").attr('class', 'd3map-legend').style("cursor", "pointer");
                        g.append("circle")
                            .attr("cx", 20)
                            .attr("cy", function(d,i){ return 20 + i*20}) // 20 is where the first dot appears. 25 is the distance between dots
                            .attr("r", 7)
                            .style("fill", function(d){ return (_options.legendeActivated.indexOf(d) > -1 ? _options.colors(d) : "transparent")})
                            .style("stroke", function(d){ return _options.colors(d)})
                            .on('click', function(d, i){
                                var circle = d3.select(this);
                                if(circle.attr("class") === "disabled"){
                                    circle.attr("class", "");
                                    circle.style("fill", function(d){ return _options.colors(d)});
                                    _options.lastLegende.push(i);
                                }else{
                                    circle.attr("class", "disabled");
                                    circle.style("fill", "transparent");
                                    if(_options.lastLegende.indexOf(i) > -1){
                                        _options.lastLegende.splice(_options.lastLegende.indexOf(i), 1)
                                    }
                                }
                                if(JSON.stringify(_options.legendeActivated.sort()) == JSON.stringify(_options.lastLegende.sort())){
                                    $("#chartBtn").hide();
                                }else{
                                    $("#chartBtn").show();
                                }
                            })
                        g.append("text")
                            .attr("x", 35)
                            .attr("y", function(d,i){ return 20 + i*20}) // 20 is where the first dot appears. 25 is the distance between dots
                            .style("fill", function(d){ return _options.colors(d)})
                            .text(function(d){ return d})
                            .attr("text-anchor", "left")
                            .style("alignment-baseline", "middle")
                            .on('click', function(d, i){
                                if(_options.clickedLegende == i){
                                    _context.clearMap()
                                    _context.addElts(_options.data);
                                }else{
                                    _options.clickedLegende = i;
                                    i = (i == 'Autre' ? '' : i);
                                    var data = Object.fromEntries(Object.entries(_options.data).
                                        filter(([key, val]) => {
                                            if(_options.dynamicLegende == false || _options.dynamicLegende == 'false'){
                                                return i == '' ? (!val[_options.legendeVar] || (val[_options.legendeVar] && val[_options.legendeVar].some(ai => _options.legende.includes(ai)) == false)) : (!val[_options.legendeVar] ? false : val[_options.legendeVar].includes(i));
                                            }else{
                                                return (i == '' ? (!val[_options.groupBy] || val[_options.groupBy] == '') : val[_options.groupBy] == i )
                                            }
                                        }))
                                    _context.addElts(data, false);
                                    }
                            })
                        return g;
                    })
                    
                    
                    d3.select("#chartBtn").on('click', function(){
                        $(".mapLoading").show();
                        _options.legendeActivated = JSON.parse(JSON.stringify(_options.lastLegende))
                        setTimeout(function(){
                            _context.addElts(data, false);
                        }, 100)
                    })
                    $("#chartBtn").hide();
                    $("#info-pane").show();
                }else{
                    $("#info-pane").hide();
                }
                _context.hideLoader();
                _map.invalidateSize();
                _context.fitBounds(data);
            }
        }
    }

    this.addGeoJSON = function(params){
        _options.arrayBounds.push(params);

    }

    this.addMarker = function(params){
        if(_options.mapCustom.addMarker){
            _options.mapCustom.addMarker(_context, params)
        }else{
            if( params.elt && params.elt.geo && params.elt.geo.latitude && params.elt.geo.longitude){
                if(!params.opt)
                    params.opt = {}
                params.opt.icon = _options.mapCustom.icon.getIcon(params)
    
                var latLon = [params.elt.geo.latitude, params.elt.geo.longitude];
                this.setDistanceToMax(latLon);
    
                var marker = L.marker(latLon, params.opt);
                _options.markerList[params.elt.id] = marker;
    
                if(_options.activePopUp){
                    _context.addPopUp(marker, params.elt)
                }
    
                _options.arrayBounds.push(latLon)
    
                if(_options.activeCluster){
                    _options.markersCluster.addLayer(marker)
                }else{
                    marker.addTo(_map);
                    if(params.center){
                        _map.panTo(latLon)
                    }
                }
    
                if(_options.mapOpt.doubleClick){
                    marker.on('click', function(e){
                        this.openPopup()
                        coInterface.bindLBHLinks();
                    })
                    marker.on('dbclick', function(e){
                        _context.centerOne(params.elt.geo.latitude, params.elt.geo.longitude)
                        coInterface.bindLBHLinks();
                    })
                }else{
                    marker.on('click', function (e) {
                        if(_options.mapOpt.onclickMarker){
                            _options.mapOpt.onclickMarker(params)
                        }else{
                            _context.centerOne(params.elt.geo.latitude, params.elt.geo.longitude)
                            coInterface.bindLBHLinks();
                        }
                    });
                }
    
                if(_options.mapOpt.mouseOver){
                    marker.on('mouseover', function (e) {
                        this.openPopup()
                        coInterface.bindLBHLinks();
                    });
                    marker.on('mouseout', function (e) {
                        var thismarker = this;
                        setTimeout(function(){
                            thismarker.closePopup();
                        }, 2000);
                    });
                }
            }
        }
    }

    this.setDistanceToMax = function(latLon){
        if(Object.keys(_options.markerList).length > 0){
            var latLonObj = L.latLng(latLon[0], latLon[1]);
            $.each(_options.markerList, function(k, v){
                var dist = latLonObj.distanceTo(v.getLatLng())
                _options.distanceToMax = (dist > _options.distanceToMax) ? dist:_options.distanceToMax
            })
        }
    }

    this.setZoomByDistance = function(){
        var markerListCount = Object.keys(_options.markerList).length
        if(markerListCount == 0){
            _options.mapOpt.zoom = 0;
            _options.mapOpt.offset.x = 0;
            _options.mapOpt.offset.y = 0;
        }else if(markerListCount == 1){
            _options.mapOpt.zoom = 14;
			_options.mapOpt.offset.x = 0.002;
			_options.mapOpt.offset.y = 0.003;
        }else{
            if(_options.distanceToMax > 9000000){
                _options.mapOpt.zoom = 2;
				_options.mapOpt.offset.x = 0;
				_options.mapOpt.offset.y = 15;
            }else if(_options.distanceToMax > 7500000){
                _options.mapOpt.zoom = 3;
				_options.mapOpt.offset.x = 0;
				_options.mapOpt.offset.y = 15;
            }else if(_options.distanceToMax > 6000000){
                _options.mapOpt.zoom = 4;
				_options.mapOpt.offset.x = 0;
				_options.mapOpt.offset.y = 0.2;
            }else if(_options.distanceToMax > 50000){
                _options.mapOpt.zoom = 6;
				_options.mapOpt.offset.x = 0;
				_options.mapOpt.offset.y = 0.2;
            }else if(_options.distanceToMax > 20000){
                _options.mapOpt.zoom = 8;
				_options.mapOpt.offset.x = 0;
				_options.mapOpt.offset.y = 0.2;
            }else if(_options.distanceToMax > 10000){
                _options.mapOpt.zoom = 10;
				_options.mapOpt.offset.x = 0;
				_options.mapOpt.offset.y = 0.2;
            }else{
                _options.mapOpt.zoom = 12;
				_options.mapOpt.offset.x = 0;
				_options.mapOpt.offset.y = 0.2;
            }
        }

        if(!_options.mapOpt.menuRight){
            _options.mapOpt.offset.x = 0;
			_options.mapOpt.offset.y = 0;
        }
    }

    this.clearMap = function(){
        if(_options.markersCluster){
            _options.markersCluster.clearLayers()
        }
        if(_options.geoJSONlayer){
            _options.geoJSONlayer.eachLayer(function(layer){
                _map.removeLayer(layer)
            });
        }
        if(Object.keys(_options.markerList).length > 0){
            $.each(_options.markerList, function(){
                _map.removeLayer(this)
            })
        }
        if(_options.markerSingleList && Object.keys(_options.markerSingleList)){
            $.each(_options.markerSingleList, function(){
                _map.removeLayer(this)
            })
        }
        _options.markerList = {};
        _options.arrayBounds = [];
        _options.bounds = null;

        _context.showLoader()
        $(_options.parentContainer + " .liste_map_element").html("")
    }

    this.addPopUp = function(marker, elt=null, open=false){
        var popupSimple = _options.mapCustom.getPopup(elt)
        marker.bindPopup(popupSimple, {width:"300px"}).openPopup();
        if(open)
            marker.openPopup();
    }

    this.centerOne = function(lat, lon){
        if(_filtres.timeoutAddCity)
            clearTimeout(_filtres.timeoutAddCity)
        _filtres.timeoutAddCity = setTimeout(function(){
            var moveLat = "0.0008";
            var moveLon = "0.000";
            
            lat = parseFloat(lat) + parseFloat(moveLat);
            lon = parseFloat(lon) + parseFloat(moveLon);
            
            var center = [lat, lon];

            _map.setView(center, _options.mapOpt.centerOneZoom)
        }, 500)
    }

    this.showLoader = function(){
        $(".mapLoading").show();
        var heightMap = $(_options.parentContainer).outerHeight();
        var widthMap = $(_options.parentContainer).outerWidth();
        if ($(_options.parentContainer + " #menuRight" + _options.parentContainerName).is(":visible")) {
			widthMap = widthMap - $(_options.parentContainer + " #menuRight" + _options.parentContainerName).outerWidth();
        }
        coInterface.showLoader(".mapLoading", trad.currentlyresearching);
        var marginLeftLoader = (widthMap - $(".mapLoading").outerWidth()) / 2;
        var marginTopLoader = (heightMap - $(".mapLoading").outerHeight()) / 2;
        $(".mapLoading").css({ "margin-left": marginLeftLoader + "px", "margin-top": marginTopLoader + "px" });
    }
    this.hideLoader = function(){
        $(".mapLoading").fadeOut();
    }

    this.setTile = function(tile){
        if (tile === "mapbox" && $('script[src="https://api.mapbox.com/mapbox.js/v3.2.1/mapbox.js"]').length){
            var accessToken = 'pk.eyJ1IjoiY29tbXVuZWN0ZXI5NzQiLCJhIjoiY2tkOHF3cTZ1MDZmNzJzbWk4M3pnZ2dvbCJ9.J9aOUObe8B9lH-8QLmDzNw';

            var mapboxTiles = L.tileLayer('https://api.mapbox.com/styles/v1/mapbox/streets-v11/tiles/{z}/{x}/{y}?access_token=' + accessToken, {
				attribution: '© <a href="https://www.mapbox.com/feedback/">Mapbox</a> © <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
            });
            _map.addLayer(mapboxTiles)
        } else if (tile === "maptiler" && $('script[src="https://api.mapbox.com/mapbox.js/v3.2.1/mapbox.js"]').length) {
            var accessToken = '2FAeSQOpVCo7WdsCmujj';

            var mapboxTiles = L.tileLayer('https://api.maptiler.com/maps/eb5fb1d0-58c0-47ab-af0a-86ed434e011c/256/{z}/{x}/{y}@2x.png?key=' + accessToken, {
                "attribution": "<a href=\"https://www.maptiler.com/copyright/\" target=\"_blank\">&copy; MapTiler</a> <a href=\"https://www.openstreetmap.org/copyright\" target=\"_blank\">&copy; OpenStreetMap contributors</a>"
            });
            _map.addLayer(mapboxTiles)
        }else if(tile === "toner-lite"){
            // L.tileLayer('https://tiles.stadiamaps.com/tiles/stamen_toner_lite/{z}/{x}/{y}{r}.png', {
			// 	attribution: '&copy; <a href="https://stadiamaps.com/" target="_blank">Stadia Maps</a> <a href="https://stamen.com/" target="_blank">&copy; Stamen Design</a> &copy; <a href="https://openmaptiles.org/" target="_blank">OpenMapTiles</a> &copy; <a href="https://www.openstreetmap.org/about" target="_blank">OpenStreetMap</a> contributors'
			// }).addTo(_map)

            var accessToken = '2FAeSQOpVCo7WdsCmujj';

			var mapboxTiles = L.tileLayer('https://api.maptiler.com/maps/toner-v2-lite/256/{z}/{x}/{y}@2x.png?key=' + accessToken, {
				"attribution": "<a href=\"https://www.maptiler.com/copyright/\" target=\"_blank\">&copy; MapTiler</a> <a href=\"https://www.openstreetmap.org/copyright\" target=\"_blank\">&copy; OpenStreetMap contributors</a>", 
			});
			_map.addLayer(mapboxTiles)
        }else if(tile === "satelite"){
            // L.tileLayer('https://tiles.stadiamaps.com/tiles/stamen_toner_lite/{z}/{x}/{y}{r}.png', {
			// 	attribution: '&copy; <a href="https://stadiamaps.com/" target="_blank">Stadia Maps</a> <a href="https://stamen.com/" target="_blank">&copy; Stamen Design</a> &copy; <a href="https://openmaptiles.org/" target="_blank">OpenMapTiles</a> &copy; <a href="https://www.openstreetmap.org/about" target="_blank">OpenStreetMap</a> contributors'
			// }).addTo(_map)

            var accessToken = '2FAeSQOpVCo7WdsCmujj';

			var mapboxTiles = L.tileLayer('https://api.maptiler.com/maps/toner-v2-lite/256/{z}/{x}/{y}@2x.png?key=' + accessToken, {
				"attribution": "<a href=\"https://www.maptiler.com/copyright/\" target=\"_blank\">&copy; MapTiler</a> <a href=\"https://www.openstreetmap.org/copyright\" target=\"_blank\">&copy; OpenStreetMap contributors</a>", 
			});
			_map.addLayer(mapboxTiles)
        }else if(tile == "mapnik"){
            L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
                maxZoom: 19,
                attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
            }).addTo(_map);
        }else{
            if(baseUrl.indexOf("www.communecter.org") > -1){
                if($('script[src="https://api.mapbox.com/mapbox.js/v3.2.1/mapbox.js"]').length){
                    var accessToken = '2FAeSQOpVCo7WdsCmujj';
                    var mapboxTiles = L.tileLayer('https://api.maptiler.com/maps/eb5fb1d0-58c0-47ab-af0a-86ed434e011c/256/{z}/{x}/{y}@2x.png?key=' + accessToken, {
                        "attribution": "<a href=\"https://www.maptiler.com/copyright/\" target=\"_blank\">&copy; MapTiler</a> <a href=\"https://www.openstreetmap.org/copyright\" target=\"_blank\">&copy; OpenStreetMap contributors</a>"
                    });
                    _map.addLayer(mapboxTiles)
                }else{
					var accessToken = '2FAeSQOpVCo7WdsCmujj';

					var mapboxTiles = L.tileLayer('https://api.maptiler.com/maps/toner-v2/256/{z}/{x}/{y}@2x.png?key=' + accessToken, {
						"attribution": "<a href=\"https://www.maptiler.com/copyright/\" target=\"_blank\">&copy; MapTiler</a> <a href=\"https://www.openstreetmap.org/copyright\" target=\"_blank\">&copy; OpenStreetMap contributors</a>",
					});

					this.map.addLayer(mapboxTiles)
                }
            }else{
                L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
                    attribution: 'Map tiles by <a href="http://stamen.com">Stamen Design</a>, under <a href="http://creativecommons.org/licenses/by/3.0">CC BY 3.0</a>. Data by <a href="http://openstreetmap.org">OpenStreetMap</a>, under <a href="http://www.openstreetmap.org/copyright">ODbL</a>'
                }).addTo(_map);
            }
        }
    }

    this.addPolygon = function(data){
        var polygon = L.polygon(data).addTo(_map)
        _context.addPopUp(polygon)
    }

    this.fitBounds = function(){
        if(_options.arrayBounds.length > 0){
            _options.arrayBounds.forEach(function(element){

            });
            _map.fitBounds(_options.arrayBounds)
            if(_map.getZoom() == 0){
                _map.setZoom(3)
            }
        }else{
            _map.panTo(_options.mapOpt.center);
            _map.setZoom(_options.mapOpt.zoom);
            _map.invalidateSize();
        }
    }
    /* public methodes ==========> */

    /* call init when constructing the object */
    init()
}