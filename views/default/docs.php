<?php

//assets from ph base repo
$cssAnsScriptFilesTheme = array(
	// SHOWDOWN
	'/plugins/showdown/showdown.min.js',
	//MARKDOWN
	'/plugins/to-markdown/to-markdown.js'
);
HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesTheme, Yii::app()->request->baseUrl);

//gettting asstes from parent module repo
$cssAnsScriptFilesModule = array( 
	'/js/dataHelpers.js',
	'/css/md.css',
);
HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, Yii::app()->getModule( Yii::app()->params["module"]["parent"] )->getAssetsUrl() );

$cssAnsScriptFilesModule = array( 
  '/leaflet/leaflet.css',
  '/leaflet/leaflet.js',
  '/css/map.css',
  '/markercluster/MarkerCluster.css',
  '/markercluster/MarkerCluster.Default.css',
  '/markercluster/leaflet.markercluster.js',
  '/js/map.js',
);
HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, Yii::app()->getModule( Map::MODULE )->getAssetsUrl() );
?>
<style type="text/css">
	table, th, td {
  border: 1px solid black;
}
table{
	width: 100%;
}
table, th, td {
  padding: 15px;
}
#doc h1 {
	margin-left: 0px;
    padding: 0px;
    margin-bottom: 30px;
    background-color: white;
    float: left;
    color: #4285f4;
    border: 0px;
    border-bottom: 1px solid #4285f4 !important;
    width: 100%;
    text-transform: none;
}
#doc h2 {
	margin-left: 0px;
    padding: 0px;
    margin-bottom: 20px;
    background-color: white;
    float: left;
    color: #e6344d;
    font-size: 28px;
    border: 0px;
    margin-top: 10px;
    width: 100%;
    text-align: left;
    text-transform: none;
}
#doc h3 {
	margin-left: 0px;
    padding: 0px;
    margin-bottom: 20px;
    background-color: white;
    float: left;
    color: #e6344d;
    font-size: 20px;
    border: 0px;
    margin-top: 10px;
    width: 100%;
    text-align: left;
    text-transform: none;
}
#doc h4 {
	margin-left: 0px;
    padding: 0px;
    margin-bottom: 20px;
    background-color: white;
    float: left;
    color: black;
    font-size: 18px;
    border: 0px;
    margin-top: 10px;
    width: 100%;
    text-align: left;
    text-transform: none;
}
#mainNav{
	display:none;
}
.menu-doc-header a{
	font-size: 16px;
	color: white;
	line-height: 50px;
}
.redirect-custom.border-right{
	border-right: 2px solid white; 
}
.redirect-custom{
	padding: 10px 50px;
}
#examples, #offers{
	display: none;
}
.redirect-custom.active{
	background-color: white;
    color: #e6344d;
    border-radius: 5px;
    margin-left: 2px;
    margin-right: 0px;
}
header{
	background-color: #e6344d;
	color: white;
	margin-bottom: 30px;
}
p{
	font-size: 14px;
}
pre{
	margin-bottom: 5px;
	color: black;
}
</style>
<header class="col-xs-12">
<h1 style="text-align: center;padding:10px; font-size:24px; text-transform: inherit;">
	<i class="fa fa-mask"></i> <?php echo CHtml::encode( (isset($this->module->pageTitle))?$this->module->pageTitle:""); ?>
</h1>
<nav class="col-xs-12 menu-doc-header text-center">
	<a href="javascript:;" class="redirect-custom border-right active" data-id="doc">
		Documentation
	</a>
	<a href="javascript:;" class="redirect-custom border-right" data-id="examples">
		Exemple
	</a>
</nav>
</header>
<div id="doc" class="col-xs-12 content-section-docs"></div>
<div id="examples" class="col-xs-12 content-section-docs">
	<div id="mainMap" style="z-index: 1; width: 100%; height: 100%; position: fixed; top: 55px; left: 0px;"></div>
</div>

<script type="text/javascript">
var map1 = {};
jQuery(document).ready(function() {
	getAjax('', baseUrl+'/<?php  echo $this->module->id;?>/default/doc',
		function(data){ 
			showdown.setOption('tables', true);
    		converter = new showdown.Converter();
    		text      = data;
    		descHtml      = converter.makeHtml(text); 
			$('#doc').html(descHtml);
		},"html");
	$(".redirect-custom").click(function(){
		$(".redirect-custom").removeClass("active");
		$(this).addClass("active");
		contentDomSection=$(this).data("id");
		$(".content-section-docs").hide(100);
		$("#"+contentDomSection).show(500);
	});
    var paramsMap = {
		zoom : 5,
        container : "mainMap",
        activePopUp : true,
        menuRight : true
    };
    map1 = mapObj.init(paramsMap);

    $("body").addClass("inSig");
	$.ajax({
		type: "POST",
		url: baseUrl+"/api/organization/get/level4/597b1c186ff992f0038b45c4/limit/3",
		dataType: "json",
		success: function(data){
			console.log("HERE", data);
			var elt = {};
			$.each(data.entities, function(k,v){
				v["type"] = v["@type"]
				elt[k] = v ;
			});
			map1.addElts(elt);

		}
	});

});

</script>
