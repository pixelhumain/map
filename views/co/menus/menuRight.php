<div id="menuRight" class="hidden-xs col-sm-3 no-padding" style="">
	<div id="menuRight_search" class="col-sm-12 no-padding">
		<!-- 	HEADER -->
		<div class="menuRight_header col-sm-12 padding-10">
			<div class="col-xs-8">
				<span class="menuRight_header_title"><?php echo Yii::t("common", "Results"); ?></span>
			<span class="right_tool_map_header_info">2 / 2</span>
			</div>
			<div class="col-xs-2">
				<button type="button" class="btn btnHeader dropdown-toggle" id="btn-filters" data-toggle="dropdown">
					<i class="fa fa-filter"></i>
				</button>
				<ul class="dropdown-menu panel_map" id="panel_filter" role="menu" aria-labelledby="panel_filter" style="overflow-y: auto; max-height: 400px;">
				    <h3 class="title_panel_map"><i class="fa fa-angle-down"></i> <?php echo Yii::t("common", "Filter results by types"); ?></h3>
					<button class='item_panel_map active' id='item_panel_filter_all'>
						<i class='fa fa-star'></i> <?php echo Yii::t("common", "All"); ?>
					</button>
				</ul>
			</div>
			<div class="col-xs-2">
				<button type="button" class="btn btnHeader dropdown-toggle" id="btn-panel" data-toggle="dropdown">
					<i class="fa fa-tags"></i>
				</button>
				<ul class="dropdown-menu panel_map pull-right" id="panel_map" role="menu" aria-labelledby="panel_map" style="overflow-y: auto; max-height: 400px;">
				    <h3 class="title_panel_map"><i class="fa fa-angle-down"></i> <?php echo Yii::t("common", "Filter results by tags"); ?></h3>
					<button class='item_panel_map active' id='item_panel_map_all'>
						<i class='fa fa-star'></i> <?php echo Yii::t("common", "All"); ?>
					</button>
				</ul>
			</div>
			

		</div>
		<div id="menuRight_search_filters" class="col-sm-12 no-padding">
			<input class="form-control date-range active" type="text" id="input_name_filter" placeholder="<?php echo Yii::t("common", "Filter by names"); ?> ...">
		</div>
		<div class="menuRight_body col-sm-12 no-padding" id="liste_map_element"></div>
	</div>
</div>