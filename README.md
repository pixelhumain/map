# Module MAP

`mapObj` sera l'objet qui va contenir tout les variables et toutes les méthodes pour instancier une map

## Pré-requis

Il faut charger les librairies suivant :

Leaflet : permet de charger le framework de carte
```
'/leaflet/leaflet.css',
'/leaflet/leaflet.js',
```

Permet de crée un cluster sur la carte
```
'/markercluster/MarkerCluster.css',
'/markercluster/MarkerCluster.Default.css',
'/markercluster/leaflet.markercluster.js',
```

Code PHP : 
```
$cssAnsScriptFilesModule = array( 
  '/leaflet/leaflet.css',
  '/leaflet/leaflet.js',
  '/css/map.css',
  '/markercluster/MarkerCluster.css',
  '/markercluster/MarkerCluster.Default.css',
  '/markercluster/leaflet.markercluster.js',
  '/js/map.js',
);
HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, Yii::app()->getModule( Map::MODULE )->getAssetsUrl() );
```


## Exemple

```
<div  id="mainMap" class="section-home col-xs-12 no-padding" style="height: 400px"></div>

<script type="text/javascript">
var map1 = {};
jQuery(document).ready(function() {
    // Parametres de la map
    var paramsMap = {
        zoom : 5,
        container : "mainMap",
        activePopUp : true,
        menuRight : true
    };
    // Création et initialisation de la map
    map1 = mapObj.init(paramsMap);
    $.ajax({
        type: "POST",
        url: baseUrl+"/api/organization/get/level4/597b1c186ff992f0038b45c4/limit/3",
        dataType: "json",
        success: function(data){
            console.log("HERE", data);
            var elt = {};
            $.each(data.entities, function(k,v){
                v["type"] = v["@type"]
                elt[k] = v ;
            });
            // On ajoute les éléments à la map
            map1.addElts(elt);
        }
    });
});
</script>
```


## Paramètre
- `map` : __Object__ Va contenir la carte généré par Leaflet. Valeur par defaut : `null`.
- `arrayBounds` : __Array__ Contient l'ensemble des bounds généré. Valeur par defaut : `[]`.
- `bounds` : Valeur par defaut : `null`.
- `markersCluster` : null,
- `markerList` : [],

## Fonction

### init(params)
Génère une copie de mapObj.

#### Paramètres
Elle prend en paramètre un objet :
- `params` : __Object__
    - `container` : __String__ va contenir l'id de la balise qui contiendra la map. Valeur par defaut :`"mapContainer"`.
    - `activeCluster` __Boolean__ Activation des clusters. Valeur par defaut :`true`.
    - `tile` : Définie le fond de la carte. Valeur par defaut : `toner`
    - `mapOpt` :  __Object__ Option lier à la carte
        + `dragging` : __Boolean__  Valeur par defaut :`true`.
        + `latLon` : __Array__ Définie la position de la carte. Valeur par defaut : `[0, 0]`.
        + `zoom` :  __Int__ Définie le zoom de la carte. Valeur par defaut : 10.
        + `activePopUp` :  __Boolean__  Active les popup des markers. Valeur par defaut :`false`.
        + `menuRight` : __Boolean__ Active la liste des éléments sur la droite. Valeur par defaut :`false`
       

### initVar
Fonction qui initialise les paramètres pour la map, elle est appelé dans la fonction  `init(params)`;
### setTile(tile)
Change le fond de la carte.
#### Paramètre
- `tile` : __String__ Définie le fond de la carte.
- `tile` : __String__ Définie le fond de la carte. Valeur par defaut : `toner`. Peut prendre plusieurs valeurs : 
    + `mapbox` : Affiche la carte de Maxbox préalable avoir charger les fichiers suivant:
        * `https://api.mapbox.com/mapbox.js/v3.2.1/mapbox.js`
        * `https://api.mapbox.com/mapbox.js/v3.2.1/mapbox.css`
    + `satelite` : Affiche la carte en vue satelite
    + `toner` : Affichage par default :, affiche la carte en nord et blanc
    + `toner-lite` : Version lite de `toner`

### addElts(data)
Fonction qui ajouter les éléments dans la carte
#### Paramètre
- `data` : __Object__ Liste des éléments a afficher sur la carte

### clearMap
Fonction qui supprime tout les éléments sur la carte

### addMarker(params)
Ajoute un marker sur la map

##### Paramètre
- `params` : __Object__
    + `elt` : Objet contenant les informations sur l'icône
        + `elt.geo.latitude` : la latitude de l'objet  
        + `elt.geo.longitude` : la longitude de l'objet
        + `elt.name` : nom de l'objet
        + `elt.type` : type l'objet
    + `center` : Définie si oui on non on soite centrer la carte sur l'icône. Prend deux valeurs `true` ou `false`. Par default `false`.
    + `opt` : Objet contenant les informations sur l'icon

### addPolygon 
### addCircle 
### addPopUp
### activeCluster
### setTile
`addMarker(tile)` : Définie le fond de la carte
##### Paramètre


### distanceTo 
calcule la distance entre 2 point
### getZoomByDistance
Donne le zoom idéal par rapport à la distance en les deux point les plus éloigner de la carte
### setZoomByDistance
Modifie le zoom par rapport à la distance en les deux point les plus éloigner de la carte 



## URL

### TileLayer 
Différents url ou on peut trouver des designs pour les cartes
- http://maps.stamen.com/#toner/12/37.7706/-122.3782
- https://stamen.com/work/maps-stamen-com/
- http://www.thunderforest.com/maps/



## TODO
- [] Pouvoir afficher plusieurs carte en meme temps
    - [X] fct create qui crée une copi de map 
    - [ ] check si une map n'est pas déjà instancier avec l'id du container
- [ ] Init css et js avec map
    - [X] Faire sur FormInMAP
    - [ ] De maniere générale
    - [ ] Parametrable a l'init 
    - [ ] Le faire en synchrone
- [ ] Intégration des GeoShape
- [ ] FormInMap
    - [ ] Rajouter le save city
    - [ ] rajouter le geoshape 
- [x] Faire en sort de changer de TileLayer 
    - [x] Test différent Tile
    - [x] Réutileser différent Tile en fct du parametre
    - [x] Changer de fond à tout moment
    - [x] Géré Mapbox quand c'est en prod
- [ ] En fct des points sur la carte, géré le zoom , plus les points sont éloigner plus on dezoom sur la carte
- [ ] Afficher les informations sur le coté de la map et non plus en popup
    - [ ] Rendre ça parametrable
- [x] Charger les éléments a l'init
- [ ] Remplacer la map de co par le module map
    - [ ] A supprimer
       - [ ] mainMap/CO2old.php 
    - [ ] A vérifier si réutilisable 
        - [ ] Sig.showIcoLoading(false);
        - [ ] Sig.preloadElementsMap 
        - [ ] Sig.showIcoLoading(false);
        - [ ] Sig.constructUI()
        - [ ] Sig.startModifyGeoposition(contextData.id, contextData.type, contextData);
        - [ ] Sig.showCityOnMap(resultNominatim, true, "person");
